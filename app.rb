require 'sinatra'
require 'sinatra/session'
require 'sinatra-websocket'
require 'bunny'
require 'net/http'
require 'json'
require 'securerandom'

set :server, 'thin'
set :bind, '0.0.0.0'
set :sockets, {}
set :flower, 'localhost'
set :mq, 'localhost'

enable :sessions
set :session_secret,  ENV.fetch('SESSION_SECRET') { SecureRandom.hex(64) }
set :session_expire,  300
set :session_fail, '/sessions/login'

EventMachine.next_tick do
  connection = Bunny.new("amqp://flower:flower@#{settings.mq}:5672")
  connection.start
  puts "Connecting to AMQP broker."

  channel  = connection.create_channel
  intervals = channel.fanout("intervals")

  channel.queue("intervals.sinatra", :auto_delete => true).bind(intervals)
    .subscribe do |delivery_info, metadata, payload|
    p = JSON.parse(payload)
    p["entity"] = "interval"
    
    if settings.sockets.key?(p["uuid"])
      settings.sockets[p["uuid"]].each { |s| s.send(JSON.dump(p)) }
    end
  end
          
  anomalies = channel.fanout("anomalies")

  channel.queue("anomalies.sinatra", :auto_delete => true).bind(anomalies)
    .subscribe do |delivery_info, metadata, payload|
    p = JSON.parse(payload)
    p["entity"] = "anomaly"
    
    if settings.sockets.key?(p["uuid"])
      settings.sockets[p["uuid"]].each { |s| s.send(JSON.dump(p)) }
    end
  end
  
end

get '/wss' do
  session!
  
  if request.websocket?
    request.websocket do |ws|
      ws.onopen do
        puts "opening websocket for: #{session['username']}"
        if !settings.sockets.has_key?(session["id"])
          settings.sockets[session["id"]] = []
        end

        settings.sockets[session["id"]].push(ws)
      end

      ws.onclose do
        settings.sockets.delete(ws)
      end
    end
  end
end

get '/map' do
  session!

  erb :index
end

get '/frequencies' do
  session!
  
  uri = URI("http://#{settings.flower}:8080/flow/frequencies/#{session[:id]}")
  Net::HTTP.get(uri)
end

get '/networks' do
  session!
  
  uri = URI("http://#{settings.flower}:8080/network/all/#{session[:id]}")
  Net::HTTP.get(uri)
end

get '/users' do
  session!

  uri = URI("http://#{settings.flower}:8080/account/#{session[:id]}")
  Net::HTTP.get(uri)
end

post '/sessions/login' do
  account = {}
  account["username"] = params[:username]
  account["password"] = params[:password]

  uri = URI("http://#{settings.flower}:8080/account/auth")
  http = Net::HTTP.new(uri.host, uri.port)
  post = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json')
  post.body = account.to_json

  resp = http.request(post)
  
  if !resp.kind_of? Net::HTTPUnauthorized
    session_start!
    session[:id] = "#{resp.body}"
    session[:username] = params[:username]
    redirect "/map"
  else
    redirect '/sessions/login'
  end
end

get '/sessions/login' do
  erb :login
end

get '/sessions/logout' do
  session_end!
  redirect '/sessions/login'
end

post '/network/create' do
  session!
  
  network = {}
  network["description"] = params[:description]
  network["address"] = params[:address]
  network["uuid"] = session[:id]

  uri = URI("http://#{settings.flower}:8080/network/create")
  http = Net::HTTP.new(uri.host, uri.port)
  post = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json')
  post.body = network.to_json

  resp = http.request(post)
  
  if resp.kind_of? Net::HTTPUnauthorized
    redirect '/sessions/login'
  end
end

post '/user/create' do
  session!

  account = {}
  account["username"] = params[:username]
  account["password"] = params[:password]
  account["organization"] = session[:id]

  uri = URI("http://#{settings.flower}:8080/account/create")
  http = Net::HTTP.new(uri.host, uri.port)
  post = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json')
  post.body = account.to_json

  resp = http.request(post)
  
  if resp.kind_of? Net::HTTPUnauthorized
    redirect '/sessions/login'
  else
    redirect '/account'
  end
end

delete '/account' do
  session!

  account = JSON.parse(request.body.read.to_s)
  account["organization"] = session[:id]

  uri = URI("http://#{settings.flower}:8080/account")
  http = Net::HTTP.new(uri.host, uri.port)
  req = Net::HTTP::Delete.new(uri, 'Content-Type' => 'application/json')
  req.body = account.to_json

  resp = http.request(req)
  
  if resp.kind_of? Net::HTTPUnauthorized
    redirect '/sessions/login'
  else
    redirect '/account'
  end
end

get '/account' do
  session!
  
  erb :account
end

post '/password' do
  session!
  
  password = {}
  password["oldPassword"] = params[:oldpass]
  password["newPassword"] = params[:newpass]
  password["username"] = session[:username]

  uri = URI("http://#{settings.flower}:8080/account/password")
  http = Net::HTTP.new(uri.host, uri.port)
  post = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json')
  post.body = password.to_json

  resp = http.request(post)
  
  if resp.kind_of? Net::HTTPUnauthorized
    redirect '/sessions/login'
  else
    redirect "/map/#{session[:id]}"
  end
end

put '/flow/:uuid' do
  data = request.body.read

  uri = URI("http://#{settings.flower}:8080/flow/#{params[:uuid]}")
  http = Net::HTTP.new(uri.host, uri.port)
  put = Net::HTTP::Put.new(uri, 'Content-Type' => 'application/json')
  put.body = data

  resp = http.request(put)
  
  if resp.kind_of? Net::HTTPUnauthorized
    redirect '/sessions/login'
  else
    resp.body
  end
end

get '/' do
  redirect '/sessions/login'
end
