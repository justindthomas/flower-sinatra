var SVG_NS = "http://www.w3.org/2000/svg"

var protocols = ["hopopt", "icmp", "igmp", "ggp", "ipip", "st", "tcp", "cbt", "egp", "igp", "bbn",
                 "nvp", "pup", "argus", "emcon", "xnet", "chaos", "udp", "mux", "dcn", "hmp",
                 "prm", "xns", "trunk1", "trunk2", "leaf1", "leaf2", "rdp", "irtp", "isotp4",
                 "netblt", "mfe-nsp", "merit-inp", "dccp", "3pc", "idpr", "xtp", "ddp", "idpr",
                 "tp++", "il", "ipv6encap", "sdrp", "ipv6-rt", "ipv6-frag", "idrp", "rsvp", "gre",
                 "dsr", "bna", "esp", "ah", "i-nlsp", "swipe", "narp", "moblie", "tlsp", "skip",
                 "ipv6-icmp", "ipv6-nonxt", "ipv6-opts", "anyhost", "cftp", "anylocal", "satnet",
                 "kryptolan", "rvd", "ippc", "anydfs", "sat-mon", "visa", "ipcu", "cpnx", "cphb",
                 "wsn", "pvp", "br-sat-mon", "sun-nd", "wb-mon", "wb-expak", "iso-ip", "vmtp",
                 "vmtps", "vines", "ttp/iptm", "nsfnet", "dgp", "tcf", "eigrp", "ospf",
                 "sprite", "larp", "mtp", "ax25", "os", "micp", "scc-sp", "etherip", "encap",
                 "anycrypto", "gmtp", "ifmp", "pnni", "pim", "aris", "scps", "qnx", "an",
                 "ipcomp", "snp", "compaq", "ipx", "vrrp", "pgm", "any0hop", "l2tp", "ddx",
                 "iatp", "stp", "srp", "uti", "smp", "sm", "ptp", "isis", "fire" ,"crtp",
                 "crudp", "sscopmce", "iplt", "sps", "pipe", "sctp", "fc", "rsvp-ete-ignore",
                 "mobility-v6", "udplite", "mpls", "manet", "hip", "shim6", "wesp", "rohc"]

var weight_ports = ["2049|6", "53|6", "53|17", "80|6", "443|6", "3260|6", "22|6", "8080|6"]

function element(name) {
    return document.getElementById(name)
}

SVGElement.prototype.getTransformToElement = SVGElement.prototype.getTransformToElement || function(toElement) {
    return toElement.getScreenCTM().inverse().multiply(this.getScreenCTM())
}

function getMatrix(matrix) {
    return $M([[matrix.a, matrix.c, matrix.e],
               [matrix.b, matrix.d, matrix.f],
               [0, 0, 1]]);
}

function setTranslationToMap(group, coordinates, yadj) {
    var size = group.getAttribute("calcwidth")
    
    var map_matrix = element('map').getCTM()
    var svg = $('svg')[0]
    var pt = svg.createSVGPoint()
    pt.x = coordinates.x
    pt.y = coordinates.y

    translated = pt.matrixTransform(map_matrix)
    
    var x = translated.x - (size / 2)
    var y = translated.y + yadj

    group.setAttribute("transform", "translate(" + x + ", " + y + ")")
}

function minWidth() {
    w = 0
    
    for(var l = 0; l < $("header li").length; l++) {
        w += $($("header li")[l]).width()
        w += parseInt($($("header li")[l]).css("padding-left"), 10)
        w += parseInt($($("header li")[l]).css("padding-right"), 10)
        w += parseInt($($("header li")[l]).css("margin-left"), 10)
        w += parseInt($($("header li")[l]).css("margin-right"), 10)
    }

    return (w + 100) + "px"
}

function updateNetworkList() {
    wf = window.flower

    $("#network-list ul").text("")

    if(wf.networks.length != 0) {
        for(var n in wf.networks) {
            $("#network-list ul").append("<li>" + wf.networks[n].address + " ("
                                         + wf.networks[n].description + ")</li>")
        }
    } else {
        $("#network-list ul").html("<li>No networks defined.</li>")
    }
}

$(document).ready(function() {
    window.flower = {}
    wf = window.flower
    
    $("#addnetwork").submit(function(evt) {
        var url = "/network/create"

        $.ajax({
            type: "POST",
            url: url,
            data: $("#addnetwork").serialize(),
            success: function(data) {
                var net = {}
                net["address"] = $("#addnetwork input[name=address]").val()
                net["description"] = $("#addnetwork input[name=description]").val()
                wf.networks.push(net)
                updateNetworkList()
                
                $("#addnetwork")[0].reset()
            }
        })

        evt.preventDefault();
        return false
    })
    
    $.get("/frequencies",function(data) {
        wf.frequencies=JSON.parse(data)
        for(var idx in weight_ports) {   
            if(!isNaN(wf.frequencies[weight_ports[idx]])) {
                wf.frequencies[weight_ports[idx]]++
            } else {
                wf.frequencies[weight_ports[idx]] = 1
            }
        }
    })
    
    $.get("/networks",function(data) {
        wf.networks=JSON.parse(data)

        updateNetworkList()
    })
    
    wf.Flow = function (dbid,
                        interval,
                        resolution,
                        ethernet_type,
                        source_address,
                        destination_address,
                        protocol,
                        source_port,
                        destination_port,
                        bytes,
                        packets) {
        this.dbid = dbid
        this.interval = interval
        this.resolution = resolution
        this.ethernet_type = ethernet_type
        this.source_address = source_address
        this.destination_address = destination_address
        this.protocol = protocol
        this.source_port = source_port
        this.destination_port = destination_port
        this.bytes = bytes
        this.packets = packets
    }

    wf.Flow.prototype = {
        getKey: function() {
            var src = this.sourceIsLocal() ? this.source_address : null
            var dst = this.destinationIsLocal() ? this.destination_address : null
            
            return (this.protocol + "|" + src + "|" + dst + "|" + this.destination_port)
        },

        isResponse: function() {
            return wf.mapController.isResponse(this.source_address,
                                               this.destination_address,
                                               this.source_port,
                                               this.destination_port,
                                               this.protocol)
        },

        sourceIsLocal: function() {
            if(!this.source_address) return false
            
            var src_local = false

            var subnetting = new wf.SubnetMath(4)

            for(var n in wf.networks) {
                if(subnetting.addressInNetwork(this.source_address, wf.networks[n].address)) {
                    src_local = true
                    break
                }
            }

            return src_local
        },

        destinationIsLocal: function() {
            if(!this.destination_address) return false
            
            var dst_local = false

            var subnetting = new wf.SubnetMath(4)

            for(var n in wf.networks) {
                if(subnetting.addressInNetwork(this.destination_address, wf.networks[n].address)) {
                    dst_local = true
                    break
                }
            }

            return dst_local
        }
    }

    wf.Point = function(x, y) {
        this.x = Math.round(x)
        this.y = Math.round(y)
    }

    wf.Node = function(coordinates, address, hostname) {
        this.coordinates = coordinates
        this.address = address
        this.hostname = hostname
        this.flows = {}
        this.volume = {
            "sent": {
                "bytes": 0,
                "packets": 0,
                "el": {}
            },
            "received": {
                "bytes": 0,
                "packets": 0,
                "el": {}
            }
        }
        this.label = {
            "el": {}
        }
    }

    wf.Node.prototype = {
        "MAX_VOLUME_RADIUS": 30,
        "NODE_SIZE_RADIUS": 3,

        updateVolume: function() {
            this.volume.sent.el.setAttribute("r", this.getSentRadius())
            this.volume.received.el.setAttribute("r", this.getReceivedRadius()) 
        },

        getSentRadius: function() {
            var percentage = Math.log(1 + this.volume.sent.bytes) / Math.log(1 + wf.mapController.bytes)

            var value = (percentage * this.MAX_VOLUME_RADIUS) + this.getReceivedRadius()
            return (!isNaN(value) && value > 0) ? value : 10
        },

        getReceivedRadius: function() {
            var percentage = Math.log(1 + this.volume.received.bytes) / Math.log(1 + wf.mapController.bytes)

            var value = percentage * this.MAX_VOLUME_RADIUS
            return (!isNaN(value) && value > 0) ? value : 5
        },

        render: function(version, network) { 
            var circle = document.createElementNS(SVG_NS, 'circle')

            circle.setAttribute("cx", this.coordinates.x)
            circle.setAttribute("cy", this.coordinates.y)
            circle.setAttribute("r", this.NODE_SIZE_RADIUS)
            if(this.broadcast) {
                circle.setAttribute("class", "broadcast")
            } else {
                circle.setAttribute("class", version)
            }
            circle.setAttribute("address", this.address)
            circle.setAttribute("resolvedName", this.hostname)
            circle.setAttribute("network", network)
            element("nodes").appendChild(circle)
            return circle
        },

        renderVolume: function(radius) {
            var volume = document.createElementNS(SVG_NS, 'circle');
            volume.setAttribute("cx", this.coordinates.x);
            volume.setAttribute("cy", this.coordinates.y);
            volume.setAttribute("r", radius);
            volume.setAttribute("address", this.address);
            element("volumes").appendChild(volume);
            return volume
        },

        renderLabel: function(address) {
            var group = window.document.createElementNS(SVG_NS, "g")
            element("labels").appendChild(group)
            
            var text = document.createElementNS(SVG_NS, 'text')
            group.appendChild(text)
            
            text.setAttribute("text-anchor", "middle")
            text.setAttribute("address", address)
            text.appendChild(document.createTextNode(address))

            text_size = text.getComputedTextLength()
            group.setAttribute("calcwidth", text_size)
            text.setAttribute("y", "15")
            text.setAttribute("x", text_size / 2)

            setTranslationToMap(group, this.coordinates, -35)

            return group
        },
		
		    moveTo: function(point) {
			      this.el.setAttribute("cx", point.x)
			      this.el.setAttribute("cy", point.y)
			      
			      this.coordinates = point
			
			      this.volume.sent.el.setAttribute("cx", point.x)
			      this.volume.sent.el.setAttribute("cy", point.y)
			      this.volume.received.el.setAttribute("cx", point.x)
			      this.volume.received.el.setAttribute("cy", point.y)

            setTranslationToMap(this.label.el, this.coordinates, -35)

            x = $($(".untracked")[0]).attr("cx")
            y = $($(".untracked")[0]).attr("cy")
            asqrd = Math.pow((Math.abs(point.x) - Math.abs(x)), 2)
            bsqrd = Math.pow((Math.abs(point.y) - Math.abs(y)), 2)
            
            wf.mapController.resizeCatcher(Math.sqrt(asqrd + bsqrd))
			
			      for(var path in wf.mapController.paths) {
				        var addresses = path.split("|")
				        if(addresses[0] == this.address || addresses[1] == this.address) {
					          wf.mapController.paths[path] = null
				        }
			      }
			
			      for(var key in this.flows) {
                if (this.flows[key].annotation != null) {
                    $(this.flows[key].annotation).remove()
                }
				        wf.mapController.repositionFlow(this.flows[key])
			      }
		    }
    }

    wf.ManagedNetwork = function(address,
                                 version,
                                 description,
                                 coordinates,
                                 radius,
                                 nodes) {
        this.address = address
        this.version = version
        this.description = description
        this.coordinates = coordinates
        this.radius = radius
        this.nodes = nodes
    }

    wf.ManagedNetwork.prototype = {
        "NODE_PLACEMENT_RADIUS": 80,
        "NODE_MARGIN": 65,
        "UNIT": 2 * Math.PI,

        "nodes": {},
        "origin": {},

        reposition: function(margin) {
            var minimum = this.radius + margin
            this.moveTo(wf.mapController.findPoint(this.origin,
                                                   minimum,
                                                   this.radius,
                                                   this.address))

            for(var address in this.nodes) {
                for(var flow in this.nodes[address].flows) {
                    wf.mapController.repositionFlow(this.nodes[address].flows[flow])
                }
            }
        },

        moveTo: function(point) {
            this.coordinates = point
            this.el.setAttribute("cx", this.coordinates.x)
            this.el.setAttribute("cy", this.coordinates.y)

            setTranslationToMap(this.label, this.coordinates, -35)
            
            for(var address in this.nodes) {
                this.nodes[address].coordinates = null
            }

            for(var address in this.nodes) {
                if(Object.keys(this.nodes).length == 1) {
                    this.nodes[address].coordinates = point
                } else {
                    this.placeNode(this.coordinates, this.nodes[address])
                }
            }
        },

        poorProximity: function(point) {
            for(var address in this.nodes) {
                var coordinates = this.nodes[address].coordinates
                if(!coordinates) {
                    return false
                }

                var distance = Math.sqrt(Math.pow(Math.abs(coordinates.x - point.x), 2) + Math.pow(Math.abs(coordinates.y - point.y), 2))
                
                if(distance < this.NODE_MARGIN + wf.Node.prototype.NODE_SIZE_RADIUS) {
                    return true
                }
            }
            return false
        },

        findPoint: function(origin, radius) {
            var original = radius

            for(var k = 1; true; k++) {
                var max_divisions = (this.UNIT * radius) / (wf.Node.prototype.NODE_SIZE_RADIUS + this.NODE_MARGIN)

                for(var divisions = 2; divisions <= max_divisions; divisions*=2) {
                    for(var i = 1; i <= divisions; i++) {
                        var position = (this.UNIT / divisions) * i + ((Math.PI/16) * k)
                        // That last addition of ((Math.PI/16) * k) shifts the points
                        // off of a line; otherwise connections tend to get muddied
                        // along the horizontal/vertical

                        var point = new wf.Point(
                            origin.x + (radius * Math.cos(position)),
                            origin.y + (radius * Math.sin(position))
                        )

                        var taken = false
                        for(var address in this.nodes) {
                            var coordinates = this.nodes[address].coordinates         
                            
                            if(!coordinates) {
                                continue
                            }

                            if(this.poorProximity(point)) {
                                taken = true
                                break
                            }
                        }

                        if(!taken) {
                            return { "point":point, "radius":radius }
                        }
                    }
                }
                radius = original * k
            }

            return null
        },

        placeNode: function(origin, node) {
            var subnetting = new wf.SubnetMath(4)

            node.coordinates = node.broadcast ?
                this.coordinates : this.findPoint(origin, this.NODE_PLACEMENT_RADIUS).point

            node.el.setAttribute("cx", node.coordinates.x)
            node.el.setAttribute("cy", node.coordinates.y)

            node.volume.sent.el.setAttribute("cx", node.coordinates.x)
            node.volume.sent.el.setAttribute("cy", node.coordinates.y)
            node.volume.sent.el.setAttribute("r", node.getSentRadius(node))

            node.volume.received.el.setAttribute("cx", node.coordinates.x)
            node.volume.received.el.setAttribute("cy", node.coordinates.y)
            node.volume.received.el.setAttribute("r", node.getReceivedRadius(node))

            setTranslationToMap(node.label.el, node.coordinates, -35)
        },

        addNode: function(address, hostname) {
            if(address in this.nodes) {
                return
            }      

            if(Object.keys(this.nodes).length == 1) {
                for(var key in this.nodes) {
                    this.placeNode(this.coordinates, this.nodes[key])
                }
            }

            var node = new wf.Node(null, address, hostname)
            var subnetting = new wf.SubnetMath(4)

            if(subnetting.addressIsBroadcast(node.address, this.address)) {
                node.broadcast = true
            }

            if(Object.keys(this.nodes).length == 0) {
                node.coordinates = this.coordinates
            } else {
                var point = this.findPoint(this.coordinates, this.NODE_PLACEMENT_RADIUS)
                node.coordinates = node.broadcast ? this.coordinates : point.point
                this.radius = point.radius + this.NODE_MARGIN
                this.el.setAttribute("r", this.radius)
            }

            if (!node.broadcast) {
                node.el = node.render(this.version, this.address)
                node.volume.sent.el = node.renderVolume(node.getSentRadius())
                node.volume.received.el = node.renderVolume(node.getReceivedRadius())
                node.label.el = node.renderLabel(node.address)
            
			          node.network = this
			          node.el.node = node
			
                $(node.el).off('mousedown')
                $(node.el).on('mousedown', this.grabNode)
			
                this.nodes[node.address] = node

                if(wf.mapController.poorProximity(this.address,
                                    this.coordinates,
                                    (this.radius + wf.mapController.NETWORK_MARGIN),
                                    wf.mapController.COORDINATES)) {
                    this.reposition(wf.mapController.NETWORK_MARGIN)
                }

                x = $($(".untracked")[0]).attr("cx")
                y = $($(".untracked")[0]).attr("cy")
                asqrd = Math.pow((Math.abs(node.coordinates.x) - Math.abs(x)), 2)
                bsqrd = Math.pow((Math.abs(node.coordinates.y) - Math.abs(y)), 2)
            
                wf.mapController.resizeCatcher(Math.sqrt(asqrd + bsqrd))
            }

            return node
        },
		
        grabNode: function(event) {
            var svg = document.querySelector('svg')
            
			      wf.selected = event.target
			
            $(document).on('mousemove', wf.ManagedNetwork.prototype.moveNode)
            $(document).on('mouseup', wf.ManagedNetwork.prototype.releaseNode)

            event.preventDefault()
        },

        releaseNode: function(event) {
            $(document).off('mousemove', wf.ManagedNetwork.prototype.moveNode)
            $(document).off('mouseup', wf.ManagedNetwork.prototype.releaseNode)

            event.preventDefault()
        },

        moveNode: function(event) {
            var svg = $('svg')[0]
            var pt = svg.createSVGPoint()
            pt.x = event.clientX
            pt.y = event.clientY
            ttm = pt.matrixTransform($('#map')[0].getCTM().inverse())
            
			      var node = wf.selected.node
			      node.moveTo(ttm)
            
            event.preventDefault()
        }
    }

    wf.MapController = function() {}

    wf.MapController.prototype = {
        "NETWORK_MARGIN": 20,
        "NETWORK_PADDING": 20,
        "MAX_FLOW_WIDTH": 100,
        "UNIT": 2 * Math.PI,
        "FLOW_MARGIN": 7,
        "COORDINATES": new wf.Point(0,0),

        // address: window.flower.ManagedNetwork()
        "networks": {},

        // flow key(): window.flower.Flow()
        "flows": {},

        // source address|destination address: largest radius
        "paths": {},

        "alerts": {},

        "bytes": 0,
        "packets": 0,
        
        grabMap: function(event) {
            if (!event.data.touch) {
                event.target.startX = event.pageX;
                event.target.startY = event.pageY;
            } else {
                let t = event.originalEvent.touches[0]

                event.target.startX = t.pageX;
                event.target.startY = t.pageY;
            }

            if(!event.data.touch) {
                $(event.target).on('mousemove', { touch: false },
                                   wf.mapController.moveMap)            
                $(event.target).on('mouseup', { touch: false },
                                   wf.mapController.releaseMap)
            } else {
                $(event.target).on('touchmove', { touch: true },
                                   wf.mapController.moveMap)            
                $(event.target).on('touchend', { touch: true },
                                   wf.mapController.releaseMap)
            }

            event.preventDefault();
        },
        
        releaseMap: function(event) {
            if(!event.data.touch) {
                $(event.target).off('mousemove', wf.mapController.moveMap)
                $(event.target).off('mouseup', wf.mapController.releaseMap)
            } else {
                $(event.target).off('touchmove', wf.mapController.moveMapTouch)
                $(event.target).off('touchend', wf.mapController.releaseMap)
            }   

            event.preventDefault();
        },

        moveMap: function(event) {
            var a
            
            if (!event.data.touch) {
                a = event
            } else {
                a = event.originalEvent.touches[0]
            }
            
            var content = element('map');
            var matrix = content.getCTM();

            var original = $M([[matrix.a, matrix.c, matrix.e],
                               [matrix.b, matrix.d, matrix.f],
                               [0, 0, 1]]);
            var moved = $M([[1, 0, a.pageX - event.target.startX],
                            [0, 1, a.pageY - event.target.startY],
                            [0, 0, 1]]);

            wf.mapController.moveGroup(a, element('annotations'));
            wf.mapController.moveGroup(a, element('labels'));
            
            event.target.startX = a.pageX;
            event.target.startY = a.pageY;

            var updated = moved.x(original);

            content.setAttribute("transform", "matrix(" +
                updated.e(1, 1) + " " + updated.e(2, 1) + " " +
                updated.e(1, 2) + " " + updated.e(2, 2) + " " +
                updated.e(1, 3) + " " + updated.e(2, 3) + ")");

            event.preventDefault();
        },

        renderAnnotation: function(event, flow) {
            var f
            
            if (event != null) {
                f = event.data.flow
            } else {
                f = flow
            }

            if (f.annotation != null) {
                $(f.annotation).remove()
            }
            
            var group = window.document.createElementNS(SVG_NS, "g")
            f.annotation = group
            
            group.setAttribute("class", "transient")
            element('annotations').appendChild(group)

            var src = f.sourceIsLocal() ? f.source_address : "external"
            var dst = f.destinationIsLocal() ? f.destination_address : "external"
                        
            var output_string = protocols[f.protocol] + " " + src + " to " + dst

            if (f.protocol == 6 || f.protocol == 17) {
                output_string += " port " + f.destination_port
            }

            var font_size = 10

            var text = window.document.createElementNS(SVG_NS, "text")
            text.setAttribute("x", "5")
            text.setAttribute("y", "15")
            var text_content = window.document.createTextNode(output_string)
            text.appendChild(text_content)

            // We have to append to the document here to get an accurate length
            // (we re-append later to bring forward)
            group.appendChild(text)
            var window_size = text.getComputedTextLength() + 12

            var container = window.document.createElementNS(SVG_NS, "path")
            container.setAttribute("d", "M " + (window_size / 2) + ",30 " +
                                   "l -5,-10 " +
                                   "l -" + ((window_size / 2) - 5) + ",0 " +
                                   "l 0,-20 " +
                                   "l " + window_size + ",0 " +
                                   "l 0,20 " +
                                   "l -" + ((window_size / 2) - 5) + ",0 " +
                                   "l -5, 10 z")

            var svg = $('svg')[0]
            var pt = svg.createSVGPoint()
            pt.x = f.label_anchor.x
            pt.y = f.label_anchor.y

            var map_matrix = element('map').getCTM()
            translated = pt.matrixTransform(map_matrix)

            var x = translated.x - (window_size / 2)
            var y = translated.y - 32

            var matrix = element('annotations').getCTM()

            group.setAttribute("calcwidth", window_size)
            group.setAttribute("transform",
                               "translate(" + (x - matrix.e) + ", " + (y - matrix.f) + ")")

            group.appendChild(container)
            group.appendChild(text)

            $(group).on('click', {flow: f}, wf.mapController.removeAnnotation)
        },

        removeAnnotation: function(event) {
            $(event.data.flow.annotation).remove()
            event.data.flow.annotation = null
        },
        
        getRadiusAngle: function(referenceX, referenceY, centerX, centerY) {
            var width = centerX - referenceX;
            var height = centerY - referenceY;
            var angle
            var radius

            if (centerY > referenceY) {
                if (centerX > referenceX) {
                    angle = Math.PI - Math.atan(Math.abs(height / width));
                    radius = Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2));
                } else if (centerX < referenceX) {
                    angle = Math.atan(Math.abs(height / width));
                    radius = Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2));
                } else if (centerX == referenceX) {
                    angle = Math.PI / 2;
                    radius = height;
                }
            } else if (centerY < referenceY) {
                if (centerX > referenceX) {
                    angle = Math.PI + Math.atan(Math.abs(height / width));
                    radius = Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2));
                } else if (centerX < referenceX) {
                    angle = (2 * Math.PI) - Math.atan(Math.abs(height / width));
                    radius = Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2));
                } else if (centerX == referenceX) {
                    angle = Math.PI * 1.5;
                    radius = Math.abs(height);
                }
            } else if (centerY == referenceY) {
                if (centerX > referenceX) {
                    angle = Math.PI;
                    radius = width;
                } else if (centerX < referenceX) {
                    angle = 0;
                    radius = Math.abs(width);
                } else if (centerX == referenceX) {
                    angle = 0;
                    radius = 0;
                }
            }

            return ({
                "radius": radius,
                "angle": angle
            })
        },

        scaleGroup: function(event, scale, tgroup, elementHeight) {
            var cX = window.innerWidth / 2;
            var cY = window.innerHeight / 2;

            if (tgroup) {
                var annotations = tgroup.childNodes;
                for (var i = 0; i < annotations.length; i++) {
                    var calcwidth = annotations[i].getAttribute("calcwidth");
                    var matrix = annotations[i].getCTM();

                    var referenceX = (matrix.e + (calcwidth / 2))
                    var referenceY = (matrix.f + elementHeight)

                    var ra = wf.mapController.getRadiusAngle(referenceX, referenceY, cX, cY);

                    var newX = (cX + ((ra.radius * scale) *
                                      Math.cos(ra.angle))) - (calcwidth / 2);
                    var newY = (cY + -((ra.radius * scale) *
                                       Math.sin(ra.angle))) - elementHeight;

                    annotations[i].setAttribute("transform",
                                                "translate(" + newX + " " + newY + ")");
                }
            }
        },

        moveGroup: function(event, tgroup) {
            if (tgroup) {
                var annotations = tgroup.childNodes;
                for (var i = 0; i < annotations.length; i++) {
                    var matrix = annotations[i].getCTM();

                    var original = $M([[matrix.a, matrix.c, matrix.e],
                                       [matrix.b, matrix.d, matrix.f],
                                       [0, 0, 1]]);
                    
                    var moved = $M([[1, 0, event.clientX - event.target.startX],
                                    [0, 1, event.clientY - event.target.startY],
                                    [0, 0, 1]]);

                    var updated = moved.x(original);
                    annotations[i].setAttribute("transform", "matrix(" +
                        updated.e(1, 1) + " " + updated.e(2, 1) + " " +
                        updated.e(1, 2) + " " + updated.e(2, 2) + " " +
                        updated.e(1, 3) + " " + updated.e(2, 3) + ")");
                }
            }
        },

        reposition: function() {
            var repositioned = {}

            for(var address in this.networks) {
                this.networks[address].coordinates = null
            }

            for(var address in this.networks) {
                this.networks[address].reposition(this.NETWORK_MARGIN)
            }
        },

        findNode: function(address) {
            for(var network_address in this.networks) {
                var node = this.networks[network_address].nodes[address]

                if(node !== undefined) {
                    return node
                }             
            }

            return null
        },

        updateFlows: function() {
            wf.mapController.paths = {}
            for(var flow in wf.mapController.flows) {
                wf.mapController.repositionFlow(wf.mapController.flows[flow])
            }
        },

        getAlertPath: function(source_node, destination_node) {
            var src = !this.findNode(source_node.address) ? null : source_node.address
            var dst = !this.findNode(destination_node.address) ? null : destination_node.address

            if(!src && !dst) {
                return null
            }
            
            // inner control curve
            var icc = this.curvePoint(source_node.coordinates,
                                  destination_node.coordinates, 0)

            var w = this.paths[src + "|" + dst] != null ?
                this.paths[src + "|" + dst] + 5 : 10
                
            // outer control curve
            var occ = this.curvePoint(source_node.coordinates, destination_node.coordinates, w)
            
            var path_commands = []

            path_commands.push("M" + source_node.coordinates.x + "," +
                               source_node.coordinates.y)
            path_commands.push("S" + icc.x + "," + icc.y + " " +
                               destination_node.coordinates.x  + "," +
                               destination_node.coordinates.y)
            path_commands.push("L" + destination_node.coordinates.x + "," +
                               destination_node.coordinates.y)
            path_commands.push("S" + occ.x + "," + occ.y + " " +
                               source_node.coordinates.x + "," +
                               source_node.coordinates.y)
            path_commands.push("Z")

            return path_commands.join(" ")
        },
        
        getPath: function(source_node, destination_node, width) {
            var src = !this.findNode(source_node.address) ? null : source_node.address
            var dst = !this.findNode(destination_node.address) ? null : destination_node.address

            if(!src && !dst) {
                return null
            }

            var ir, icc, occ, mcc, mpx, mpy
            var mid_point = null;
            
            if(width > 0) {
                // inner radius
                ir = !this.paths[src + "|" + dst] ? 3 :
                    this.paths[src + "|" + dst] + this.FLOW_MARGIN

                // inner control curve
                icc = this.curvePoint(source_node.coordinates,
                                      destination_node.coordinates, ir)

                // outer control curve
                occ = this.curvePoint(source_node.coordinates,
                                      destination_node.coordinates, ir + width)

                // middle control curve (for annotation anchor)
                mcc = this.curvePoint(source_node.coordinates,
                                      destination_node.coordinates, ir + (width / 3))

                // t = 0.5; // given example value
                // x = (1 - t) * (1 - t) * p[0].x + 2 * (1 - t) * t * p[1].x + t * t * p[2].x;
                // y = (1 - t) * (1 - t) * p[0].y + 2 * (1 - t) * t * p[1].y + t * t * p[2].y;
                mpx = (source_node.coordinates.x/4) + (mcc.x/2) +
                    (destination_node.coordinates.x/4)
                mpy = (source_node.coordinates.y/4) + (mcc.y/2) +
                    (destination_node.coordinates.y/4)

                mid_point = new wf.Point(mpx, mpy)
            } else {
                ir = 0

                // inner control curve
                icc = this.curvePoint(source_node.coordinates,
                                      destination_node.coordinates, ir)

                var w = this.paths[src + "|" + dst] != null ?
                    this.paths[src + "|" + dst] : 10
                
                // outer control curve
                occ = this.curvePoint(source_node.coordinates,
                                      destination_node.coordinates,
                                      w)
            }
            
            var path_commands = []

            path_commands.push("M" + source_node.coordinates.x + "," +
                               source_node.coordinates.y)
            path_commands.push("S" + icc.x + "," + icc.y + " " +
                               destination_node.coordinates.x  + "," +
                               destination_node.coordinates.y)
            path_commands.push("L" + destination_node.coordinates.x + "," +
                               destination_node.coordinates.y)
            path_commands.push("S" + occ.x + "," + occ.y + " " +
                               source_node.coordinates.x + "," +
                               source_node.coordinates.y)
            path_commands.push("Z")

            if(width != null) {
                this.paths[src + "|" + dst] = ir + width
            }

            return [path_commands.join(" "), mid_point]
        },

        getFlowWidth: function(flow) {
            var bytes_percentage = (1 + flow.bytes) / this.bytes

            if(bytes_percentage < .1) {
                bytes_percentage = .1
            }

            return bytes_percentage * this.MAX_FLOW_WIDTH
        },

        repositionFlow: function(flow) {
            var src = flow.sourceIsLocal() ? flow.source_address : null
            var dst = flow.destinationIsLocal() ? flow.destination_address : null
            
            var sourceNode, destinationNode
            
            sourceNode = this.findNode(src) ||
                new wf.Node(this.COORDINATES, null)
            destinationNode = this.findNode(dst) ||
                new wf.Node(this.COORDINATES, null)

            if(!flow.el) {
                return
            }

            var path = this.getPath(sourceNode, destinationNode, this.getFlowWidth(flow))
            flow.label_anchor = path[1]
            flow.el.setAttribute("d", path[0])
            
            if (flow.annotation != null) this.renderAnnotation(null, flow)

            if(this.alerts[src + "|" + dst] != null) {
                var alert_path = this.getAlertPath(sourceNode, destinationNode)
                this.alerts[src + "|" + dst].setAttribute("d", alert_path)
                $(this.alerts[src + "|" + dst]).on('click', {al: this.alerts[src + "|" + dst]}, wf.mapController.hideAlert)
            }
        },

        hideAlert: function(event) {
            $(event.data.al).hide()
        },

        renderFlow: function(flow) {
            if(flow.getKey() in this.flows && this.flows[flow.getKey()].el === undefined) {
                var src = flow.sourceIsLocal() ? flow.source_address : null
                var dst = flow.destinationIsLocal() ? flow.destination_address : null
                
                var sourceNode, destinationNode, port

                sourceNode = this.findNode(src) || new wf.Node(this.COORDINATES, null)
                destinationNode = this.findNode(dst) || new wf.Node(this.COORDINATES, null)
                port = flow.destination_port

                if(sourceNode.address === null && destinationNode.address === null) {
                    return
                }

                sourceNode.flows[flow.getKey()] = flow
                destinationNode.flows[flow.getKey()] = flow

                var path = this.getPath(sourceNode, destinationNode, this.getFlowWidth(flow))

                flow.label_anchor = path[1]
                flow.annotation = null
                
                var rendered = document.createElementNS(SVG_NS, 'path')
                rendered.setAttribute("d", path[0])
                if(sourceNode.address) {
                    rendered.setAttribute("source", sourceNode.address) }
                if(destinationNode.address) {
                    rendered.setAttribute("destination", destinationNode.address) }
                rendered.setAttribute("port", port)
                rendered.setAttribute("class", protocols[flow.protocol])
                element("flows").appendChild(rendered)

                $(rendered).on('click', { flow: flow }, this.renderAnnotation)

                flow.el = rendered
            }
        },

        renderAlert: function(flow) {
            var src = flow.sourceIsLocal() ? flow.source_address : null
            var dst = flow.destinationIsLocal() ? flow.destination_address : null
                
            if(flow.getKey() in this.flows && this.alerts[src + "|" + dst] === undefined) {
                var sourceNode, destinationNode

                sourceNode = this.findNode(src) || new wf.Node(this.COORDINATES, null)
                destinationNode = this.findNode(dst) || new wf.Node(this.COORDINATES, null)

                if(sourceNode.address === null && destinationNode.address === null) {
                    return
                }

                sourceNode.flows[flow.getKey()] = flow
                destinationNode.flows[flow.getKey()] = flow

                var path = this.getPath(sourceNode, destinationNode)
                
                var rendered = document.createElementNS(SVG_NS, 'path')
                rendered.setAttribute("d", path[0])
                if(sourceNode.address) {
                    rendered.setAttribute("source", sourceNode.address) }
                if(destinationNode.address) {
                    rendered.setAttribute("destination", destinationNode.address) }
                rendered.setAttribute("class", "alert")
                element("flows").appendChild(rendered)

                this.alerts[src + "|" + dst] = rendered
            }
        },

        addNetworksAndNodes: function(flow) {
            var math = new wf.SubnetMath(4)
            for(var i = 0; i < wf.networks.length; i++) {
                var network = wf.networks[i]
                if(math.addressInNetwork(flow.source_address, network.address)) {
                    wf.mapController.addNetwork(network.address, network.description, "ipv4")
                    wf.mapController.networks[network.address].addNode(
                        flow.source_address)
                }

                if(math.addressInNetwork(flow.destination_address, network.address)) {
                    wf.mapController.addNetwork(network.address, network.description, "ipv4")
                    wf.mapController.networks[network.address].addNode(
                        flow.destination_address)
                }
            }
        },

        addFlow: function(flow) {
            this.addNetworksAndNodes(flow)
            this.updateFrequencies(flow)
            this.updateNodeCounters(flow)
            this.updateFlowCounters(flow)
            this.updateMapCounters(flow)

            for(var network in this.networks) {
                for(var node in this.networks[network].nodes) {
                    this.networks[network].nodes[node].updateVolume()
                }
            }

            this.renderAlert(wf.mapController.flows[flow.getKey()])
            this.renderFlow(wf.mapController.flows[flow.getKey()])
            this.updateFlows()
        },

        updateFrequencies: function(flow) {
            if(flow.protocol == 6 || flow.protocol == 17) {
                var source = flow.source_port + "|" + flow.protocol
                var destination = flow.destination_port + "|" + flow.protocol

                if(isNaN(wf.frequencies[source])) {
                    wf.frequencies[source] = 0
                }

                if(isNaN(wf.frequencies[destination])) {
                    wf.frequencies[destination] = 0
                }

                wf.frequencies[source]++
                wf.frequencies[destination]++
            }
        },

        updateNodeCounters: function(flow) {
            var source_node = this.findNode(flow.source_address)
            var destination_node = this.findNode(flow.destination_address)

            if(source_node !== null) {
                source_node.volume.sent.bytes += Math.round(flow.bytes)
                source_node.volume.sent.packets += Math.round(flow.packets)
            }

            if(destination_node !== null) {
                destination_node.volume.received.bytes += Math.round(flow.bytes)
                destination_node.volume.received.packets += Math.round(flow.packets)
            }
        },

        updateMapCounters: function(flow) {
            this.bytes += Math.round(flow.bytes)
            this.packets += Math.round(flow.packets)
        },

        updateFlowCounters: function(flow) {
            var key = flow.getKey()
            if(key in wf.mapController.flows) {
                var existing = wf.mapController.flows[key]
                existing.bytesSent += flow.bytes
                existing.packetsSent += flow.packets
                existing.bytes += flow.bytes
                existing.packets += flow.packets
            } else {
                flow.bytesSent = flow.bytes
                flow.packetsSent = flow.packets
                flow.bytesReceived = 0
                flow.packetsReceived = 0

                wf.mapController.flows[key] = flow
            }
        },

        isResponse: function(source_address,
                             destination_address,
                             source_port,
                             destination_port,
                             protocol) {

            var source = wf.frequencies[source_port + "|" + protocol]
            var destination = wf.frequencies[destination_port + "|" + protocol]

            if(source === undefined) { source = 0 }
            if(destination === undefined) { destination = 0 }

            return true ? source > destination : false
        },

        curvePoint: function(source, destination, radius) {
            if(source.y == destination.y) {
                if(source.x < destination.x) {
                    return new wf.Point(source.x + (Math.abs(destination.x - source.x) / 2),
                                        source.y - radius)
                }

                if(source.x > destination.x) {
                    return new wf.Point(destination.x + (Math.abs(source.x - destination.x) / 2),
                                        source.y + radius)
                }
            } else if(source.x == destination.x) {
                if(source.y < destination.y) {
                    return new wf.Point(source.x + radius,
                                        source.y + (Math.abs(destination.y - source.y) / 2))
                }

                if(source.y > destination.y) {
                    return new wf.Point(source.x - radius,
                                        destination.y + (Math.abs(source.y - destination.y) / 2))
                }
            } else {
                var opposite = (destination.y - source.y) / 2
                var adjacent = (destination.x - source.x) / 2

                var clock = Math.PI / 2
                var counter = 3 * clock

                var unit
                if((opposite < 0 && adjacent > 0) || (opposite > 0 && adjacent > 0)) {
                    unit = counter
                } else if ((opposite < 0 && adjacent < 0) || (opposite > 0 && adjacent < 0)) {
                    unit = clock
                }

                // theta = angle between bisection of the circle and the trailing half
                // of the flow line add PI/2 to find the point on the line orthogonal to
                // and bisecting the flow line
                var theta = Math.atan(opposite / adjacent) + unit
                var x = (radius * Math.cos(theta)) + adjacent + source.x
                var y = (radius * Math.sin(theta)) + opposite + source.y

                return new wf.Point(x,y)
            }
        },

        renderNetwork: function(network) {
            var background = document.createElementNS(SVG_NS, 'circle')
            background.setAttribute("cx", network.coordinates.x)
            background.setAttribute("cy", network.coordinates.y)
            background.setAttribute("r", network.radius)
            background.setAttribute("class", network.version)
            background.setAttribute("network", network.address)
            element("networks").appendChild(background)

            var group = window.document.createElementNS(SVG_NS, "g")
            element("labels").appendChild(group)

            var text = document.createElementNS(SVG_NS, 'text')
            text.setAttribute("text-anchor", "middle")
            text.setAttribute("network", network.address)
            text.setAttribute("class", "network")
            text.appendChild(document.createTextNode(network.description))
            group.appendChild(text)
            
            var container_size = text.getComputedTextLength() + 12
            group.setAttribute("calcwidth", container_size)

            text.setAttribute("y", "15")
            text.setAttribute("x", container_size / 2)
            
            var container = document.createElementNS(SVG_NS, "rect")
            container.setAttribute("width", container_size)
            container.setAttribute("height", 20)
            container.setAttribute("x", "0")
            container.setAttribute("y", "0")

            setTranslationToMap(group, network.coordinates, 20)
            
            group.appendChild(container)
            group.appendChild(text)

            return { "el": background, "label": group }
        },

        poorProximity: function(evaluating, point, padding, origin) {
            var distance = Math.sqrt(Math.pow(Math.abs(origin.x - point.x), 2) +
                                     Math.pow(Math.abs(origin.y - point.y), 2))
            if(distance < this.NETWORK_MARGIN + padding) {
                return true
            }

            for(var address in this.networks) {
                if(address == evaluating) {
                    continue;
                }

                var coordinates = this.networks[address].coordinates
                if(!coordinates) {
                    return false
                }

                var distance = Math.sqrt(Math.pow(Math.abs(coordinates.x - point.x), 2) +
                                         Math.pow(Math.abs(coordinates.y - point.y), 2))
                
                var adjusted_padding = padding
                if(this.networks[address].radius) {
                    adjusted_padding += this.networks[address].radius
                }

                if(distance < this.NETWORK_MARGIN + adjusted_padding) {
                    return true
                }
            }
            return false
        },

        findPoint: function(origin, radius, padding, address) {
            for(var k = 1; true; k++) {
                var max_divisions = (this.UNIT * radius)

                for(var divisions = 2; divisions <= max_divisions; divisions*=2) {
                    for(var i = 1; i <= divisions; i++) {
                        var position = (this.UNIT / divisions) * i

                        var point = new wf.Point(
                            origin.x + (radius * Math.cos(position)),
                            origin.y + (radius * Math.sin(position))
                        )

                        if(this.poorProximity(address,
                                              point,
                                              (padding + this.NETWORK_MARGIN),
                                              origin)) {
                            continue
                        }

                        return point
                    }
                }
                radius++
            }

            return null
        },

        addNetwork: function(address, description, version) {
            if(address in this.networks) {
                return;
            }

            var network_location = this.findPoint(this.COORDINATES,
                                                  (this.NETWORK_PADDING + this.NETWORK_MARGIN),
                                                  this.NETWORK_PADDING, address)
            var network =
                new wf.ManagedNetwork(address,
                                      version,
                                      description,
                                      new wf.Point(network_location.x, network_location.y),
                                      this.NETWORK_PADDING, {})

            var rendered = this.renderNetwork(network)
            network.origin = this.COORDINATES
            network.el = rendered.el
            network.label = rendered.label

            this.networks[network.address] = network

            return network
        },

        scaleMap: function(event, delta) {
            if(delta == null) {
                delta = 1.0
            }
            
            var content = element('map')
            var matrix = content.getCTM()

            var original = $M([[matrix.a, matrix.c, matrix.e],
                               [matrix.b, matrix.d, matrix.f],
                               [0, 0, 1]])

            var scaled = $M([[delta, 0, 0],
                             [0, delta, 0],
                             [0, 0, 1]])


            var shiftX = ((1 - delta) * window.innerWidth) / 2;
            var shiftY = ((1 - delta) * window.innerHeight) / 2;

            var preshift = $M([[1, 0, shiftX], [0, 1, shiftY], [0, 0, 1]])
            var updated = preshift.x(scaled.x(original))

            content.setAttribute("transform", "matrix(" +
                updated.e(1, 1) + " " + updated.e(2, 1) + " " +
                updated.e(1, 2) + " " + updated.e(2, 2) + " " +
                updated.e(1, 3) + " " + updated.e(2, 3) + ")");

            wf.mapController.scaleGroup(event, delta, element('annotations'), 32);
            wf.mapController.scaleGroup(event, delta, element('labels'), 35);
        },

        initializeMap: function() {
            var map = element("map")
            var mapSvg = map.parentNode

            $('#catcher').off('mousewheel')
            $('#catcher').on('mousewheel', wf.sliderController.captureScroll)
            $('#catcher').off('DOMMouseScroll')
            $('#catcher').on('DOMMouseScroll', wf.sliderController.captureScroll)

            $('#catcher').off('mousedown')
            $('#catcher').on('mousedown', { touch: false }, this.grabMap)
            $('#catcher').off('touchstart')
            $('#catcher').on('touchstart', { touch: true }, this.grabMap)

            map.setAttribute("transform", "translate(0 0)")

            var windowHeight = window.innerHeight;
            var windowWidth = window.innerWidth;

            var mapHeight = mapSvg.width.baseVal.value;
            var mapWidth = mapSvg.height.baseVal.value;

            var leftShift = 0;
            var topShift = 0;
            
            // Determine width offset to center
            if (mapWidth > windowWidth) {
                leftShift = windowWidth / 2;
            }

            // Determine height offset to center
            if (mapHeight > windowHeight) {
                topShift = windowHeight / 2;
            }

            var matrix = map.getCTM();

            var original = $M([[matrix.a, matrix.c, matrix.e],
                               [matrix.b, matrix.d, matrix.f],
                               [0, 0, 1]]);
            
            var moved = $M([[1, 0, leftShift],
                            [0, 1, topShift],
                            [0, 0, 1]]);

            var updated = moved.x(original);

            map.setAttribute("transform", "matrix(" +
                updated.e(1, 1) + " " + updated.e(2, 1) + " " +
                updated.e(1, 2) + " " + updated.e(2, 2) + " " +
                updated.e(1, 3) + " " + updated.e(2, 3) + ")");
        },

        resizeCatcher: function(minimum) {
            current = $("#catcher").attr("r")

            if(current < minimum) {
                $("#catcher").attr("r", minimum)
            }
        }
    }

    wf.SubnetMath = function(version) {
        if(version == 4) {
            return new wf.SubnetMath.v4
        }
    }

    wf.SubnetMath.v4 = function() {}

    wf.SubnetMath.v4.prototype = {
        addressToBase10: function(address) {
            var octets = address.split(".")
            var binary_octets = []
            for(var i = 0; i < octets.length; i++) {
                binary_octets.push(this.octetToBinary(octets[i]))
            }

            return this.bitsToBase10(binary_octets[0].concat(binary_octets[1],
                                                             binary_octets[2],
                                                             binary_octets[3]))
        },

        octetToBinary: function(integer) {
            var places = [ 128,64,32,16,8,4,2,1 ]
            var output = []
            for(var i = 0; i < places.length; i++) { 
                if (integer < places[i]) { 
                    output.push(0) 
                } else {  
                    integer = integer - places[i] 
                    output.push(1)
                }
            }

            return output
        },

        bitsToBase10: function(arr) {
            var places = [  2147483648,
                            1073741824,
                            536870912,
                            268435456,
                            134217728,
                            67108864,
                            33554432,
                            16777216,
                            8388608,
                            4194304,
                            2097152,
                            1048576,
                            524288,
                            262144,
                            131072,
                            65536,
                            32768,
                            16384,
                            8192,
                            4096,
                            2048,
                            1024,
                            512,
                            256,
                            128,
                            64,
                            32,
                            16,
                            8,
                            4,
                            2,
                            1 ]
            var output = 0
            for(var i = 0; i < arr.length; i++) { 
                if (arr[i]) { 
                    output = output + places[i] 
                }
            }

            return output
        },

        cidrToBase10: function(cidr) {
            var bits = []
            for(var i = 0; i < 32; i++) {
                if(i < cidr) {
                    bits.push(1)
                } else {
                    bits.push(0)
                }
            }
            return this.bitsToBase10(bits)
        },

        addressInNetwork: function(address, networkCidr) {
            var network = networkCidr.split("/")[0]
            var cidr = networkCidr.split("/")[1]
            var cidrBase10 = this.cidrToBase10(cidr)

            if((this.addressToBase10(address) &
                cidrBase10) == (this.addressToBase10(network) & cidrBase10)) {
                return true
            }

            return false
        },

        addressIsMulticast: function(address) {
            var mc_cidr = "224.0.0.0/4"

            return this.addressInNetwork(address, mc_cidr)
        },
        
        addressIsBroadcast: function(address, networkCidr) {
            var global_broadcast = address == "255.255.255.255" ? true : false
            var local_broadcast = false
            
            if(!this.addressInNetwork(address, networkCidr)) {
                local_broadcast = false
            } else {
                var cidr = networkCidr.split("/")[1]
                var address_broadcast = this.addressToBase10(address) << cidr >>> cidr
                local_broadcast = address_broadcast == ~this.cidrToBase10(cidr) ? true : false
            }
            
            return (global_broadcast || local_broadcast)
        }
    }

    wf.isBroadMulticast = function (address) {
        var subnetting = new wf.SubnetMath(4)

        ret = false
            
        for(var n in wf.networks) {
            if(ret = subnetting.addressIsBroadcast(address, wf.networks[n].address)) {
                break
            }
        }
        
        return (ret || subnetting.addressIsMulticast(address))
    },

    wf.SliderController = function() { 
        this.slider_position = 0
    }

    wf.SliderController.prototype = {
        initializeSlider: function() {
            this.slider_xd1 = 0
            this.slider_yd1 = 0
        },

        grabSlider: function(event) {
            var slider_handle = document.getElementById('slider')

            var matrix = slider_handle.getTransformToElement(slider_handle.parentNode)

            wf.sliderController.slider_xd1 = parseFloat(matrix.e)
            wf.sliderController.slider_yd1 = parseFloat(matrix.f) - event.clientY
            wf.sliderController.slider_startY = event.clientY

            $(window).on('mousemove', wf.sliderController.moveSlider)
            $(window).on('mouseup', wf.sliderController.releaseSlider)

            event.preventDefault();
        },

        wheelDelta: function(event) {
            return event.originalEvent.detail ?
                event.originalEvent.detail * -1: event.originalEvent.wheelDelta / 40;
        },

        rotateMap: function(event) {
            if ((event.type == "mousewheel") || (event.type == "DOMMouseScroll")) {
                var wheelDelta = this.wheelDelta(event);
                var rotation = 0;
                if (wheelDelta > 0) {
                    rotation = -3;
                }
                if (wheelDelta < 0) {
                    rotation = 3;
                }

                var content = document.getElementById('map');
                var matrix = content.getCTM();
                
                var pointerX,
                    pointerY;

                var leftVal = content.parentNode.parentNode.offsetLeft;
                var topVal = content.parentNode.parentNode.offsetTop;
                var parent = content.parentNode.parentNode.offsetParent;

                while (parent != null) {
                    leftVal += parent.offsetLeft;
                    topVal += parent.offsetTop;
                    parent = parent.offsetParent;
                }

                pointerX = event.clientX - leftVal;
                pointerY = event.clientY - topVal;

                var radians = rotation * (Math.PI / 180);
                var cos = Math.cos(radians);
                var sin = Math.sin(radians);
                var original = $M([[matrix.a, matrix.c, matrix.e],
                                   [matrix.b, matrix.d, matrix.f],
                                   [0, 0, 1]]);

                var preshift = $M([[1, 0, -pointerX],
                                   [0, 1, -pointerY],
                                   [0, 0, 1]]);
                
                var rotated = $M([[cos, -sin, 0],
                                  [sin, cos, 0],
                                  [0, 0, 1]]);
                
                var postshift = $M([[1, 0, pointerX],
                                    [0, 1, pointerY],
                                    [0, 0, 1]]);

                var updated = postshift.x(rotated.x(preshift.x(original)));

                // Rotate annotations
                wf.sliderController.rotateGroup(event, pointerX, pointerY,
                                                radians, element('annotations'));
                wf.sliderController.rotateGroup(event, pointerX, pointerY,
                                                radians, element('labels'));

                content.setAttribute("transform", "matrix(" +
                                     updated.e(1, 1) + " " + updated.e(2, 1) + " " +
                                     updated.e(1, 2) + " " + updated.e(2, 2) + " " +
                                     updated.e(1, 3) + " " + updated.e(2, 3) + ")");

                event.preventDefault();
            }
        },

        rotateGroup: function(event, pointerX, pointerY, radians, tgroup) {
            if (tgroup) {
                var annotations = tgroup.childNodes;
                for (var i = 0; i < annotations.length; i++) {
                    var calcwidth = annotations[i].getAttribute("calcwidth");
                    var matrix = annotations[i].getCTM();

                    var referenceX = (matrix.e + (calcwidth / 2));
                    var referenceY = (matrix.f + 32);

                    var ra = wf.mapController.getRadiusAngle(referenceX, referenceY,
                                                             pointerX, pointerY);
                    
                    var current_angle = ra.angle;
                    var radius = ra.radius

                    var newX,
                        newY;
                    if (radius == 0) {
                        newX = matrix.e;
                        newY = matrix.f;
                    } else {
                        var new_angle = current_angle + -radians;

                        newX = (pointerX + (radius * Math.cos(new_angle))) - (calcwidth / 2);
                        newY = (pointerY + -(radius * Math.sin(new_angle))) - 32;
                    }

                    annotations[i].setAttribute("transform",
                                                "translate(" + newX + " " + newY + ")");
                }
            }
        },

        captureScroll: function(event) {
            if (event.altKey) {
                wf.sliderController.rotateMap(event);
            } else {
                wf.sliderController.moveSlider(event)
            }
        },

        moveSlider: function(event) {
            var delta
            var dep_x
            var dep_y

            dep_x = wf.sliderController.slider_xd1

            if ((event.type == "mousewheel") || (event.type == "DOMMouseScroll")) {
                var wheelDelta = wf.sliderController.wheelDelta(event)
                if (wheelDelta > 0) {
                    dep_y = wf.sliderController.slider_yd1 + 1
                    delta = 0.95
                }
                if (wheelDelta < 0) {
                    dep_y = wf.sliderController.slider_yd1 - 1
                    delta = 1.05
                }
            } else {
                delta = 1 - ((event.clientY - wf.sliderController.slider_startY) * 0.05)
                wf.sliderController.slider_startY = event.clientY

                // slider_position holds the persistent y-axis offset pixel position of the
                // slider handle note that if  this is not a mousewheel event, slider_position
                // is updated after the 25+/-pixel constraint check further down.
                dep_y = wf.sliderController.slider_yd1 + event.clientY
            }

            wf.sliderController.slider_position = dep_y

            if (! (dep_y > 20) && !(dep_y < -20)) {
                wf.mapController.scaleMap(event, delta)
                event.preventDefault()
            }
        }
    }

    wf.mapController = new wf.MapController    
    wf.sliderController = new wf.SliderController
    
    wf.mapController.initializeMap()
    wf.sliderController.initializeSlider()

    var interval = function(i) {
        for(var key in i.flows) {
            var flow = i.flows[key]
            var source_address = flow.source
            var destination_address = flow.destination

            var packets,bytes,ethernet_type,protocol,source_port,destination_port
            for(var count in flow.count) {
                var key = count.split(":")
                if(key[0].toLowerCase() == "byte") {
                    bytes = flow.count[count]
                } else if(key[0].toLowerCase() == "packet") {
                    packets = flow.count[count]
                }
                
                ethernet_type = key[1]
                protocol = key[2]
                source_port = key[3]
                destination_port = key[4]
            }

            var flow_obj = new wf.Flow(i.id,
                                       i.interval,
                                       i.resolution,
                                       ethernet_type,
                                       source_address,
                                       destination_address,
                                       protocol,
                                       source_port,
                                       destination_port,
                                       bytes,
                                       packets)

            if((flow_obj.source_address != null && wf.isBroadMulticast(flow_obj.source_address)) ||
               (flow_obj.destination_address != null && wf.isBroadMulticast(flow_obj.destination_address))) {
                return
            }
                
            if ((flow_obj.protocol == 6 || flow_obj.protocol == 17) && flow_obj.isResponse()) {
                flow_obj.source_address = destination_address;
                flow_obj.source_port = destination_port;
                flow_obj.destination_address = source_address;
                flow_obj.destination_port = source_port;
                flow_obj.bytesReceived = bytes
                flow_obj.packetsReceived = packets
                flow_obj.bytesSent = 0
                flow_obj.packetsSent = 0
            }
                
            wf.mapController.addFlow(flow_obj)
            $("#timestamp").text(new Date().toString())
        }
    }

    var anomaly = function(a) {
        var flow_obj = new wf.Flow(null,
                                   null,
                                   null,
                                   null,
                                   a.source,
                                   a.destination)

        var src = flow_obj.sourceIsLocal() ? flow_obj.source_address : null
        var dst = flow_obj.destinationIsLocal() ? flow_obj.destination_address : null

        if(wf.mapController.alerts[src + "|" + dst] !== undefined) {
            $(wf.mapController.alerts[src + "|" + dst]).show()
        }
    }
    
    var show = function(el) {
        return function(msg) {
            var message = JSON.parse(msg)

            if(message.entity == "interval") {
                interval(message)
            } else if(message.entity == "anomaly") {
                anomaly(message)
            }
        }
    }(element('msgs'))

    var p = location.protocol == 'http:' ? 'ws://' : 'wss://'        
    var ws = new WebSocket(p + window.location.host + "/wss")
    ws.onmessage = function(m) { show(m.data) }
});
