var SVG_NS = "http://www.w3.org/2000/svg";

var MapRequester = Class.create({
    initialize: function () {
        
    },
    
    getMap: function (constraints) {
        this.clear();
        element('maploader').style.visibility = "visible";
        element('slider-group').style.visibility = "hidden";
        
        if(constraints) {
            this.getData(constraints);
        } else {
            this.getData("hours 1");
        }
    },

    getData: function (constraints, tracker) {
        var url = CONTEXT_ROOT + '/mapdata';

        var post;
        
        if(constraints) {
            post = 'constraints=' + Base64.encode(constraints)
        } else if(tracker) {
            post = 'tracker=' + tracker;
        }
        
        var mapRequester = this;
        new Ajax.Request(url, {
            method: 'post',
            postBody: post,
            onSuccess: function(transport) {
                mapRequester.buildMap(transport);
            }
        });
    },

    buildMap: function(transport) {
        if (transport.responseText != null) {
            try {
                var resp = JSON.parse(transport.responseText);
                if(resp.finished) {
                    mapBuilder = new MapBuilder(resp);
                    mapController = new MapController(resp.untracked);
                } else {
                    var mapRequester = this;
                    setTimeout(function(){
                        mapRequester.getData(null, resp.tracker);
                    }, 3000);
                }
            } catch(e) {
                alert(e);
            }
        }
    },
    
    clear: function() { 
        //console.log("Clearing map...");
        
        while(element("nodes").hasChildNodes()) element("nodes").removeChild(element("nodes").firstChild);
        while(element("labels").hasChildNodes()) element("labels").removeChild(element("labels").firstChild);
        while(element("networks").hasChildNodes()) element("networks").removeChild(element("networks").firstChild);
        while(element("flows").hasChildNodes()) element("flows").removeChild(element("flows").firstChild);
        while(element("volumes").hasChildNodes()) element("volumes").removeChild(element("volumes").firstChild);
        while(element("annotations").hasChildNodes()) element("annotations").removeChild(element("annotations").firstChild);
    }
})

var MapBuilder = Class.create({
    "NETWORK_MARGIN": 20,
    "NETWORK_PADDING": 20,
    "MAX_VOLUME_RADIUS": 50,
    "NODE_MARGIN": 30,
    "NODE_RADIUS": 3,

    initialize: function(data) {
        this.data = data;

        this.positionNodes();
        this.positionNetworks();

        this.weightFlows();

        var map = this.nodeMap();
        this.weightNodes(map);

        this.positionFlows(map);

        this.render(map);
    },

    weightNodes: function(node_map) {
        var largest = 0;

        for (var node in node_map) {
            if (!node_map.hasOwnProperty(node)) {
                continue;
            }

            node_map[node].bytesReceived = 0;
            node_map[node].bytesSent = 0;

            for (var flow_key in this.data.flows) {
                if (!this.data.flows.hasOwnProperty(flow_key)) {
                    continue;
                }

                var flow = this.data.flows[flow_key];
                if (flow.sourceAddress == node) {
                    node_map[node].bytesReceived += parseInt(flow.bytesReceived);
                    node_map[node].bytesSent += parseInt(flow.bytesSent);
                }
                if (flow.destinationAddress == node) {
                    node_map[node].bytesReceived += parseInt(flow.bytesSent);
                    node_map[node].bytesSent += parseInt(flow.bytesReceived);
                }
            }

            if ((node_map[node].bytesReceived + node_map[node].bytesSent) > largest) {
                largest = (node_map[node].bytesReceived + node_map[node].bytesSent);
            }
        }

        for (node in node_map) {
            if (!node_map.hasOwnProperty(node)) {
                continue;
            }
            
            var received = node_map[node].bytesReceived;
            var sent = node_map[node].bytesSent;

            node_map[node].inbound = this.NODE_RADIUS + ((received / largest) * this.MAX_VOLUME_RADIUS);
            node_map[node].outbound = node_map[node].inbound + this.NODE_RADIUS + ((sent / largest) * this.MAX_VOLUME_RADIUS);
        }
    },

    weightFlows: function() {
        var largest = 0;
        for (var flow_key in this.data.flows) {
            if (!this.data.flows.hasOwnProperty(flow_key)) {
                continue;
            }
            
            var flow = this.data.flows[flow_key];
            if ((parseInt(flow.bytesReceived) + parseInt(flow.bytesSent)) > largest) {
                largest = parseInt(flow.bytesReceived) + parseInt(flow.bytesSent);
            }
        }

        for (flow_key in this.data.flows) {
            if (!this.data.flows.hasOwnProperty(flow_key)) {
                continue;
            }
            flow = this.data.flows[flow_key];
            flow.weight = log1p((parseInt(flow.bytesReceived) + parseInt(flow.bytesSent))) / log1p(largest);
        }
    },

    positionFlows: function(node_map) {
        var radius_map = {};
        var r = 20;
        for (var flow_key in this.data.flows) {
            if (!this.data.flows.hasOwnProperty(flow_key)) {
                continue;
            }
            
            var flow = this.data.flows[flow_key];
            
            if(flow.sourceAddress == flow.destinationAddress) {
                continue;
            }
            
            if (!radius_map[flow.sourceAddress]) {
                radius_map[flow.sourceAddress] = {};
            }
                
            if (!radius_map[flow.sourceAddress][flow.destinationAddress]) { 
                radius_map[flow.sourceAddress][flow.destinationAddress] = r
            } else {
                radius_map[flow.sourceAddress][flow.destinationAddress] += r;
            }

            flow.start = {};
            flow.control = {};
            flow.end = {};

            if (node_map[flow.sourceAddress]) {
                flow.version = node_map[flow.sourceAddress].version;
                flow.start.x = node_map[flow.sourceAddress].x;
                flow.start.y = node_map[flow.sourceAddress].y;
            } else {
                flow.start.x = 0;
                flow.start.y = 0;
            }

            if (node_map[flow.destinationAddress]) {
                if (!flow.version) {
                    flow.version = node_map[flow.destinationAddress].version;
                }
                flow.end.x = node_map[flow.destinationAddress].x;
                flow.end.y = node_map[flow.destinationAddress].y
            } else {
                flow.end.x = 0;
                flow.end.y = 0;
            }

            var xdif = (flow.end.x - flow.start.x) / 2;
            var ydif = (flow.end.y - flow.start.y) / 2;

            var right = Math.PI / 2;
            var left = 3 * right;

            var unit;
            if (xdif < 0 || (xdif == 0 && ydif > 0)) {
                unit = right;
            } else if (xdif > 0 || (xdif == 0 && ydif < 0)) {
                unit = left;
            }

            var theta = Math.atan(ydif / xdif) + (3 * unit);
            flow.control.x = (radius_map[flow.sourceAddress][flow.destinationAddress] * Math.cos(theta)) + flow.start.x + xdif;
            flow.control.y = (radius_map[flow.sourceAddress][flow.destinationAddress] * Math.sin(theta)) + flow.start.y + ydif;
        }
    },

    nodeMap: function() {
        var map = {};

        if (this.data.untracked) {
            map[this.data.untracked] = {
                "x": 0,
                "y": 0
            }
        }

        for (var network_key in this.data.networks) {
            if (!this.data.networks.hasOwnProperty(network_key)) {
                continue;
            }
            var network = this.data.networks[network_key];

            for (var node_key in network.nodes) {
                if (!network.nodes.hasOwnProperty(node_key)) {
                    continue;
                }
                
                var node = network.nodes[node_key];
                map[node.address] = {
                    "x": (node.x + network.x),
                    "y": (node.y + network.y),
                    "version": network.version
                };
            }
        }
        return map;
    },

    render: function(node_map) {        
        if (this.data.untracked) {
            var untracked = document.createElementNS(SVG_NS, 'circle');
            untracked.id = generateID(this.data.untracked);
            untracked.setAttributeNS(null, "cx", node_map[this.data.untracked].x);
            untracked.setAttributeNS(null, "cy", node_map[this.data.untracked].y);
            untracked.setAttributeNS(null, "r", this.NODE_RADIUS);
            untracked.setAttributeNS(null, "class", "untracked");
            untracked.setAttributeNS(null, "address", this.data.untracked);
            untracked.setAttributeNS(null, "network", this.data.untracked);
            element("nodes").appendChild(untracked);

            var text = document.createElementNS(SVG_NS, 'text');
            text.id = generateID("text-" + this.data.untracked);
            text.setAttributeNS(null, "x", 0);
            text.setAttributeNS(null, "y", -15);
            text.setAttributeNS(null, "text-anchor", "middle");
            text.setAttributeNS(null, "class", "untracked");
            text.appendChild(document.createTextNode(this.data.untracked));
            element("labels").appendChild(text);
        }

        for (var network_key in this.data.networks) {
            if (!this.data.networks.hasOwnProperty(network_key)) {
                continue;
            }
            var network = this.data.networks[network_key];
            
            if(!network.nodes || network.nodes.length == 0) { 
                continue;
            }
            
            var background = document.createElementNS(SVG_NS, 'circle');
            background.id = generateID(network.address);
            background.setAttributeNS(null, "cx", network.x);
            background.setAttributeNS(null, "cy", network.y);
            background.setAttributeNS(null, "r", network.radius + this.NETWORK_PADDING);
            background.setAttributeNS(null, "class", network.version);
            background.setAttributeNS(null, "network", network.address);
            element("networks").appendChild(background);

            text = document.createElementNS(SVG_NS, 'text');
            text.id = generateID("text-" + network.address);
            text.setAttributeNS(null, "x", network.x);
            text.setAttributeNS(null, "y", network.y - 15);
            text.setAttributeNS(null, "text-anchor", "middle");
            text.setAttributeNS(null, "network", network.address);
            text.appendChild(document.createTextNode(network.name));
            element("labels").appendChild(text);
            
            for (var node_key in network.nodes) {
                if (!network.nodes.hasOwnProperty(node_key)) {
                    continue;
                }
                var node = network.nodes[node_key];
                var circle = document.createElementNS(SVG_NS, 'circle');
                circle.id = generateID(node.address);
                circle.setAttributeNS(null, "cx", node.x + network.x);
                circle.setAttributeNS(null, "cy", node.y + network.y);
                circle.setAttributeNS(null, "r", this.NODE_RADIUS);
                if(node.address == network.broadcast) {
                    circle.setAttributeNS(null, "class", "broadcast");
                } else {
                    circle.setAttributeNS(null, "class", network.version);
                }
                circle.setAttributeNS(null, "address", node.address);
                circle.setAttributeNS(null, "resolvedName", node.resolvedName);
                circle.setAttributeNS(null, "network", network.address);
                element("nodes").appendChild(circle);
            }
        }

        for (var flow_key in this.data.flows) {
            if (!this.data.flows.hasOwnProperty(flow_key)) {
                continue;
            }
            
            var flow = this.data.flows[flow_key];
            
            if(flow.sourceAddress == flow.destinationAddress) { 
                continue;
            }
            
            var path = document.createElementNS(SVG_NS, 'path');
            path.setAttributeNS(null, "d", "M" + flow.start.x + "," + flow.start.y + " S" + flow.control.x + "," + flow.control.y + " " + flow.end.x + "," + flow.end.y);
            path.setAttributeNS(null, "style", "opacity:" + flow.weight);
            path.setAttributeNS(null, "class", flow.version);
            path.setAttributeNS(null, "source", flow.sourceAddress);
            path.setAttributeNS(null, "destination", flow.destinationAddress);
            path.setAttributeNS(null, "protocol", flow.protocol);
            if (flow.port) {
                path.setAttributeNS(null, "port", flow.port);
            }
            path.setAttributeNS(null, "bytes-sent", flow.bytesSent);
            path.setAttributeNS(null, "bytes-received", flow.bytesReceived);
            element("flows").appendChild(path);
        }

        for (node in node_map) {
            if (!node_map.hasOwnProperty(node)) {
                continue;
            }
            
            if (!isNaN(node_map[node].outbound)) {
                var outbound = document.createElementNS(SVG_NS, 'circle');
                outbound.id = generateID("outbound-" + node);
                outbound.setAttributeNS(null, "cx", node_map[node].x);
                outbound.setAttributeNS(null, "cy", node_map[node].y);
                outbound.setAttributeNS(null, "r", node_map[node].outbound);
                outbound.setAttributeNS(null, "address", node);
                element("volumes").appendChild(outbound);
            }

            if (!isNaN(node_map[node].inbound)) {
                var inbound = document.createElementNS(SVG_NS, 'circle');
                inbound.id = generateID("inbound-" + node);
                inbound.setAttributeNS(null, "cx", node_map[node].x);
                inbound.setAttributeNS(null, "cy", node_map[node].y);
                inbound.setAttributeNS(null, "r", node_map[node].inbound);
                inbound.setAttributeNS(null, "address", node);
                element("volumes").appendChild(inbound);
            }
        }
        element('maploader').style.visibility = "hidden";
        element('map').style.visibility = ("visible");
    },

    positionNetworks: function() {
        var unit = 2 * Math.PI;
        var divisions = 6;

        var network_array = new Array();
        for (var network_key in this.data.networks) {
            if (!this.data.networks.hasOwnProperty(network_key)) {
                continue;
            }
            
            if (!this.data.networks[network_key].nodes || this.data.networks[network_key].nodes.length == 0) {
                continue;
            }
            
            var network = this.data.networks[network_key];
            network.x = 0;
            network.y = 0;
            network_array.push(network);
        }

        var positioned_array = new Array();
        var radius = 150;
        while (network_array.length > 0) {
            for (var i = 1; i < (divisions + 1) && network_array.length > 0; i++) {
                network = network_array.shift();
                
                var p = (unit / divisions) * i;

                if (positioned_array.length == 0) {
                    network.x = radius * Math.cos(p);
                    network.y = radius * Math.sin(p);
                } else {
                    var this_radius = radius;
                    for (var j = 0; j < positioned_array.length; j++) {
                        var positioned = positioned_array[j];
                        var too_close = true;

                        while (too_close) {
                            network.x = this_radius * Math.cos(p);
                            network.y = this_radius * Math.sin(p);

                            var xdif = Math.abs(positioned.x - network.x);
                            var ydif = Math.abs(positioned.y - network.y);

                            var distance = Math.sqrt(Math.pow(xdif, 2) + Math.pow(ydif, 2));
                            if (distance < (this.NETWORK_MARGIN + (2 * this.NETWORK_PADDING) + positioned.radius + network.radius)) {
                                this_radius += 5;
                            } else {
                                too_close = false;
                            }
                        }
                    }
                }
                positioned_array.push(network);
            }
            divisions *= 2;
            radius += 50;
        }
    },

    positionNodes: function() {
        for (var network_key in this.data.networks) {
            if (!this.data.networks.hasOwnProperty(network_key)) {
                continue;
            }
            
            var network = this.data.networks[network_key];
            
            if(!network.nodes || network.nodes.length == 0) {
                continue;
            }
            
            var node;
            
            if (network.nodes.length == 1) {
                network.radius = (this.NODE_MARGIN / 2) + (2 * this.NODE_RADIUS);
                node = network.nodes[0];
                node.x = 0;
                node.y = 0;
                node.r = this.NODE_RADIUS;
            } else if (network.nodes.length > 1) {
                network.radius = (this.NODE_MARGIN / 2) + (2 * this.NODE_RADIUS);
                if (network.broadcast) {
                    network.radius = this.NODE_MARGIN + (2 * this.NODE_RADIUS);
                }

                var unit = 2 * Math.PI;
                var divisions = 6;
                var node_array = new Array();
                for (var node_key in network.nodes) {
                    if (!network.nodes.hasOwnProperty(node_key)) {
                        continue;
                    }
                    node = network.nodes[node_key];
                    node_array.push(node);
                }

                while (node_array.length > 0) {
                    for (var i = 1; i < (divisions + 1) && node_array.length > 0;) {
                        node = node_array.shift();
                        if(node.address != network.broadcast) {
                            p = (unit / divisions) * i++;
                            node.x = network.radius * Math.cos(p);
                            node.y = network.radius * Math.sin(p);
                        } else {
                            node.x = 0;
                            node.y = 0;
                        }
                    }

                    divisions *= 2;
                    if(node_array.length > 0) {
                        network.radius += (this.NODE_MARGIN + (2 * this.NODE_RADIUS));
                    }
                }
            }
        }
    }
})

var MapController = Class.create({
    initialize: function(untracked) {
        var mapController = this;

        this.initializeSlider();
        this.initializeMap();
        this.untracked = untracked;

        Event.stopObserving($('map'),'mousewheel');
        Event.observe($('map'), 'mousewheel', this.captureScroll.bindAsEventListener(this, mapController));
        Event.stopObserving($('map'),'DOMMouseScroll');
        Event.observe($('map'), 'DOMMouseScroll', this.captureScroll.bindAsEventListener(this, mapController));

        Event.stopObserving($('map'),'mousedown');
        Event.observe($('map'), 'mousedown', this.grabMap.bindAsEventListener(this, mapController));
        Event.stopObserving($('map'),'dblclick');
        Event.observe($('map'), 'dblclick', this.resetMap.bindAsEventListener(this, mapController));

        Event.stopObserving($('slider'), 'mousedown');
        Event.observe($('slider'), 'mousedown', this.grabSlider.bindAsEventListener(this, mapController));

        this.attachListeners(this, $$('#flows > path'), mapController);
        this.attachListeners(this, $$('#nodes > circle'), mapController);
    },

    initializeSlider: function() {
        this.slider_position = 0;
        $('slider').setAttributeNS(null, "transform", "translate(0 0)");

        var matrix = $('slider').getTransformToElement($('slider').parentNode);
        this.slider_xd1 = parseFloat(matrix.e);
        this.slider_yd1 = parseFloat(matrix.f);
        
        element('slider-group').style.visibility = 'visible';
    },

    initializeMap: function() {
        this.displayNodes = new Array();
        var map = element('map');
        var mapSvg = map.parentNode;
        var mapDiv = mapSvg.parentNode;

        map.setAttributeNS(null, "transform", "translate(0 0)");
        var windowHeight,
        windowWidth;

        if (mapDiv) {
            windowHeight = mapDiv.offsetHeight;
            windowWidth = mapDiv.offsetWidth;
        } else {
            windowHeight = window.innerHeight;
            windowWidth = window.innerWidth;
        }

        var w = mapSvg.width;
        var h = mapSvg.height;

        var mapHeight = h.baseVal.value;
        var mapWidth = w.baseVal.value;

        var leftShift;
        var topShift;
        // Determine width offset to center
        if (mapWidth > windowWidth) {
            leftShift = windowWidth / 2;
        }

        // Determine height offset to center
        if (mapHeight > windowHeight) {
            topShift = windowHeight / 2;
        }

        var matrix = map.getCTM();

        var original = $M([[matrix.a, matrix.c, matrix.e], [matrix.b, matrix.d, matrix.f], [0, 0, 1]]);
        var moved = $M([[1, 0, leftShift], [0, 1, topShift], [0, 0, 1]]);

        var updated = moved.x(original);

        map.setAttribute("transform", "matrix(" +
            updated.e(1, 1) + " " + updated.e(2, 1) + " " +
            updated.e(1, 2) + " " + updated.e(2, 2) + " " +
            updated.e(1, 3) + " " + updated.e(2, 3) + ")");
    },

    attachListeners: function(event, elements, mapController) {
        elements.each(function(item) {
            if (item.getAttribute("address")) {
                if(item.getAttribute("address") != this.untracked) {
                    Event.stopObserving(item,'click');
                    Event.observe(item, 'click', mapController.selectNode.bindAsEventListener(event));
                }
            } else {
                Event.stopObserving(item,'click');
                Event.observe(item, 'click', mapController.drawDetail.bindAsEventListener(event));
                Event.stopObserving(item,'contextmenu');
                Event.observe(item, 'contextmenu', mapController.getContext.bindAsEventListener(event));
            }

            Event.stopObserving(item,'mouseover');
            Event.observe(item, 'mouseover', mapController.setText.bindAsEventListener(event));
        }.bind(this));
    },

    getContext: function(event) {
        alert("Context");
    },

    drawDetail: function(event) {
        var group = window.document.createElementNS(SVG_NS, "g");
        group.setAttribute("class", "transient");
        element('annotations').appendChild(group);

        var output_string = event.element().getAttribute('source')
        + " to " + event.element().getAttribute('destination');

        if (event.element().getAttribute('port')) {
            output_string += " port " + event.element().getAttribute('port');
        }

        var font_size = 10;

        var text = window.document.createElementNS(SVG_NS, "text");
        text.setAttribute("x", "5");
        text.setAttribute("y", "15");
        var text_content = window.document.createTextNode(output_string);
        text.appendChild(text_content);

        // We have to append to the document here to get an accurate length (we re-append later to bring forward)
        group.appendChild(text);
        var window_size = text.getComputedTextLength() + 12;

        var container = window.document.createElementNS(SVG_NS, "path");
        container.setAttribute("d", "M " + (window_size / 2) + ",30 l -5,-10 l -" + ((window_size / 2) - 5) + ",0 l 0,-20 l " + window_size + ",0 l 0,20 l -" + ((window_size / 2) - 5) + ",0 l -5, 10 z");

        if ($('main')) {
            var x = event.clientX - $("main").offsetLeft - (window_size / 2);
            var y = event.clientY - $("main").offsetTop - $('volume-container').offsetHeight - 43;
        } else {
            x = event.clientX - (window_size / 2);
            y = event.clientY - 32;
        }

        var matrix = element('annotations').getCTM();

        group.setAttribute("calcwidth", window_size);
        group.setAttribute("transform", "translate(" + (x - matrix.e) + ", " + (y - matrix.f) + ")");
        group.setAttribute("id", generateID("annotation-" + event.element().getAttribute('source') + event.element().getAttribute('destination') + event.element().getAttribute('port')));
        group.appendChild(container);
        group.appendChild(text);
    },

    selectNode: function(event) {
        this.displayNodes.push(event.element());
        this.refine();
    },

    setText: function(event) {
        if ($('inspector')) {
            var fields = $('inspector').getElementsByTagName("span");

            if (fields.length > 7) {
                if (event.element().getAttribute("address")) {
                    fields[0].innerHTML = event.element().getAttribute("address");
                    fields[0].style.display = 'block';
                    fields[0].parentNode.parentNode.parentNode.style.display = 'block';
                } else {
                    fields[0].innerHTML = '';
                    fields[0].style.display = 'none';
                    fields[0].parentNode.parentNode.parentNode.style.display = 'none';
                }

                if (event.element().getAttribute("resolvedName")) {
                    fields[1].innerHTML = event.element().getAttribute("resolvedName");
                    fields[1].style.display = 'block';
                    fields[1].parentNode.parentNode.parentNode.style.display = 'block';
                } else {
                    fields[1].innerHTML = '';
                    fields[1].style.display = 'none';
                    fields[1].parentNode.parentNode.parentNode.style.display = 'none';
                }

                if (event.element().getAttribute("source")) {
                    fields[2].innerHTML = event.element().getAttribute("source");
                    fields[2].style.display = 'block';
                    fields[2].parentNode.parentNode.parentNode.style.display = 'block';
                } else {
                    fields[2].innerHTML = '';
                    fields[2].style.display = 'none';
                    fields[2].parentNode.parentNode.parentNode.style.display = 'none';
                }

                if (event.element().getAttribute("destination")) {
                    fields[3].innerHTML = event.element().getAttribute("destination");
                    fields[3].style.display = 'block';
                    fields[3].parentNode.parentNode.parentNode.style.display = 'block';
                } else {
                    fields[3].innerHTML = '';
                    fields[3].style.display = 'none';
                    fields[3].parentNode.parentNode.parentNode.style.display = 'none';
                }

                if (event.element().getAttribute("port")) {
                    fields[4].innerHTML = event.element().getAttribute("port");
                    fields[4].style.display = 'block';
                    fields[4].parentNode.parentNode.parentNode.style.display = 'block';
                } else {
                    fields[4].innerHTML = '';
                    fields[4].style.display = 'none';
                    fields[4].parentNode.parentNode.parentNode.style.display = 'none';
                }

                if (event.element().getAttribute("protocol")) {
                    fields[5].innerHTML = event.element().getAttribute("protocol");
                    fields[5].style.display = 'block';
                    fields[5].parentNode.parentNode.parentNode.style.display = 'block';
                } else {
                    fields[5].innerHTML = '';
                    fields[5].style.display = 'none';
                    fields[5].parentNode.parentNode.parentNode.style.display = 'none';
                }

                if (event.element().getAttribute("bytes-sent")) {
                    fields[6].innerHTML = event.element().getAttribute("bytes-sent");
                    fields[6].style.display = 'block';
                    fields[6].parentNode.parentNode.parentNode.style.display = 'block';
                } else {
                    fields[6].innerHTML = '';
                    fields[6].style.display = 'none';
                    fields[6].parentNode.parentNode.parentNode.style.display = 'none';
                }

                if (event.element().getAttribute("bytes-received")) {
                    fields[7].innerHTML = event.element().getAttribute("bytes-received");
                    fields[7].style.display = 'block';
                    fields[7].parentNode.parentNode.parentNode.style.display = 'block';
                } else {
                    fields[7].innerHTML = '';
                    fields[7].style.display = 'none';
                    fields[7].parentNode.parentNode.parentNode.style.display = 'none';
                }
            }
        }
    },

    refine: function() {
        var i = 0;

        // Hide all flow path elements
        var pathElements = element("flows").getElementsByTagName("path");
        for (i = 0; i < pathElements.length; i++) {
            // Only paths representing a network flow will have a "source" - the slider will not, for example
            pathElements[i].style.visibility = "hidden";
        }

        // Hide all annotation group elements
        var annotationGroups = element("annotations").getElementsByTagName("g");
        for (i = 0; i < annotationGroups.length; i++) {
            annotationGroups[i].style.visibility = "hidden";
        }

        var networkID;
        // Hide all node circle elements
        var circleElements = element("nodes").getElementsByTagName("circle");
        for (i = 0; i < circleElements.length; i++) {
            circleElements[i].style.visibility = "hidden";
        }

        // Hide all network circle elements
        var networkElements = element("networks").getElementsByTagName("circle");
        for (i = 0; i < networkElements.length; i++) {
            networkElements[i].style.visibility = "hidden";
        }

        // Hide all volume circle elements
        var volumeElements = element("volumes").getElementsByTagName("circle");
        for (i = 0; i < volumeElements.length; i++) {
            volumeElements[i].style.visibility = "hidden";
        }

        // Hide all labels
        var labelElements = element("labels").getElementsByTagName("text");
        for (i = 0; i < labelElements.length; i++) {
            labelElements[i].style.visibility = "hidden";
        }

        var nodes = new Array();
        // Display any node that's in the displayNodes array
        for (i = 0; i < this.displayNodes.length; i++) {
            this.displayNodes[i].style.visibility = "visible";
            element(generateID("inbound-" + this.displayNodes[i].getAttribute("address"))).style.visibility = "visible";
            element(generateID("outbound-" + this.displayNodes[i].getAttribute("address"))).style.visibility = "visible";
            networkID = generateID(this.displayNodes[i].getAttribute("network"));
            if (networkID) {
                element(networkID).style.visibility = "visible";

                var textID = generateID('text-' + this.displayNodes[i].getAttribute("network"));
                if (element(textID)) {
                    element(textID).style.visibility = "visible";
                }
            }

            nodes.push(this.displayNodes[i].getAttribute("address"));
        }

        // Loop through the pathElements array to find paths and nodes that should be visible
        for (i = 0; i < pathElements.length; i++) {
            var k = 0;
            for (k = 0; k < this.displayNodes.length; k++) {
                var h = 0;
                if (pathElements[i].getAttribute("source") == this.displayNodes[k].getAttribute("address")) {
                    pathElements[i].style.visibility = "visible";

                    if (element(generateID("annotation-" + pathElements[i].getAttribute('source') + pathElements[i].getAttribute('destination') + pathElements[i].getAttribute('port')))) {
                        element(generateID("annotation-" + pathElements[i].getAttribute('source') + pathElements[i].getAttribute('destination') + pathElements[i].getAttribute('port'))).style.visibility = "visible";
                    }

                    // We re-use the circleElements object defined above
                    for (h = 0; h < circleElements.length; h++) {
                        if (circleElements[h].getAttribute("address") == pathElements[i].getAttribute("destination")) {
                            circleElements[h].style.visibility = "visible";

                            element(generateID(circleElements[h].getAttribute("network"))).style.visibility = "visible";
                            element(generateID("text-" + circleElements[h].getAttribute("network"))).style.visibility = "visible";

                            element(generateID("inbound-" + circleElements[h].getAttribute("address"))).style.visibility = "visible";
                            element(generateID("outbound-" + circleElements[h].getAttribute("address"))).style.visibility = "visible";
                        }
                    }
                }
                if (pathElements[i].getAttribute("destination") == this.displayNodes[k].getAttribute("address")) {
                    if (element(generateID("annotation-" + pathElements[i].getAttribute('source') + pathElements[i].getAttribute('destination') + pathElements[i].getAttribute('port')))) {
                        element(generateID("annotation-" + pathElements[i].getAttribute('source') + pathElements[i].getAttribute('destination') + pathElements[i].getAttribute('port'))).style.visibility = "visible";
                    }

                    pathElements[i].style.visibility = "visible";
                    for (h = 0; h < circleElements.length; h++) {
                        if (circleElements[h].getAttribute("address") == pathElements[i].getAttribute("source")) {
                            circleElements[h].style.visibility = "visible";

                            element(generateID(circleElements[h].getAttribute("network"))).style.visibility = "visible";
                            element(generateID("text-" + circleElements[h].getAttribute("network"))).style.visibility = "visible";

                            element(generateID("inbound-" + circleElements[h].getAttribute("address"))).style.visibility = "visible";
                            element(generateID("outbound-" + circleElements[h].getAttribute("address"))).style.visibility = "visible";
                        }
                    }
                }
            }
        }

        if (window.flowRequester) {
            flowRequester.refine(nodes);
        }
    },

    resetMap: function(event, mapController) {
        this.releaseMap(event, mapController);
        var i = 0;
        this.displayNodes = new Array();

        // Show all network elements
        var networkElements = element('networks').getElementsByTagName('circle');
        for (i = 0; i < networkElements.length; i++) {
            networkElements[i].style.visibility = 'visible';
        }

        // Show all flow path elements
        var pathElements = element('flows').getElementsByTagName('path');
        for (i = 0; i < pathElements.length; i++) {
            pathElements[i].style.visibility = 'visible';
        }

        // Show all node elements
        var nodeElements = element('nodes').getElementsByTagName('circle');
        for (i = 0; i < nodeElements.length; i++) {
            nodeElements[i].style.visibility = 'visible';
        }

        // Show all volume elements
        var volumeElements = element('volumes').getElementsByTagName('circle');
        for (i = 0; i < volumeElements.length; i++) {
            volumeElements[i].style.visibility = 'visible';
        }

        // Show all label elements
        var textElements = element('labels').getElementsByTagName('text');
        for (i = 0; i < textElements.length; i++) {
            textElements[i].style.visibility = 'visible';
        }

        // Show all annotations
        var annotationElements = element('annotations').getElementsByTagName('g');
        for (i = 0; i < annotationElements.length; i++) {
            annotationElements[i].style.visibility = 'visible';
        }

        if (window.flowRequester) {
            flowRequester.reset();
        }
    },

    grabMap: function(event, mapController) {
        this.map_startX = event.clientX;
        this.map_startY = event.clientY;

        this.boundMove = this.moveMap.bindAsEventListener(this, mapController);
        this.boundRelease = this.releaseMap.bindAsEventListener(this, mapController);
        var move = this.boundMove;
        var release = this.boundRelease;

        Event.observe(window, 'mousemove', move);
        Event.observe(window, 'mouseup', release);

        event.preventDefault();
    },

    releaseMap: function(event, mapController) {
        Event.stopObserving(window, 'mousemove', mapController.boundMove);
        Event.stopObserving(window, 'mouseup', mapController.boundRelease);
        event.preventDefault();
    },

    moveMap: function(event) {
        var content = element('map');
        var matrix = content.getCTM();

        var original = $M([[matrix.a, matrix.c, matrix.e], [matrix.b, matrix.d, matrix.f], [0, 0, 1]]);
        var moved = $M([[1, 0, event.clientX - this.map_startX], [0, 1, event.clientY - this.map_startY], [0, 0, 1]]);

        this.moveAnnotations(event);

        this.map_startX = event.clientX;
        this.map_startY = event.clientY;

        var updated = moved.x(original);

        content.setAttribute("transform", "matrix(" +
            updated.e(1, 1) + " " + updated.e(2, 1) + " " +
            updated.e(1, 2) + " " + updated.e(2, 2) + " " +
            updated.e(1, 3) + " " + updated.e(2, 3) + ")");

        event.preventDefault();
    },

    getRadiusAngle: function(referenceX, referenceY, centerX, centerY) {
        var width = centerX - referenceX;
        var height = centerY - referenceY;
        var angle,
        radius;

        if (centerY > referenceY) {
            if (centerX > referenceX) {
                angle = Math.PI - Math.atan(Math.abs(height / width));
                radius = Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2));
            } else if (centerX < referenceX) {
                angle = Math.atan(Math.abs(height / width));
                radius = Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2));
            } else if (centerX == referenceX) {
                angle = Math.PI / 2;
                radius = height;
            }
        } else if (centerY < referenceY) {
            if (centerX > referenceX) {
                angle = Math.PI + Math.atan(Math.abs(height / width));
                radius = Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2));
            } else if (centerX < referenceX) {
                angle = (2 * Math.PI) - Math.atan(Math.abs(height / width));
                radius = Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2));
            } else if (centerX == referenceX) {
                angle = Math.PI * 1.5;
                radius = Math.abs(height);
            }
        } else if (centerY == referenceY) {
            if (centerX > referenceX) {
                angle = Math.PI;
                radius = width;
            } else if (centerX < referenceX) {
                angle = 0;
                radius = Math.abs(width);
            } else if (centerX == referenceX) {
                angle = 0;
                radius = 0;
            }
        }

        return ({
            "radius": radius,
            "angle": angle
        })
    },

    rotateAnnotations: function(event, pointerX, pointerY, radians) {
        var tgroup = element('annotations');

        if (tgroup) {
            var annotations = tgroup.childNodes;
            for (var i = 0; i < annotations.length; i++) {
                var calcwidth = annotations[i].getAttribute("calcwidth");
                var matrix = annotations[i].getCTM();

                var referenceX = (matrix.e + (calcwidth / 2));
                var referenceY = (matrix.f + 32);

                var ra = this.getRadiusAngle(referenceX, referenceY, pointerX, pointerY);

                var current_angle = ra.angle;
                var radius = ra.radius

                var newX,
                newY;
                if (radius == 0) {
                    newX = matrix.e;
                    newY = matrix.f;
                } else {
                    var new_angle = current_angle + -radians;

                    newX = (pointerX + (radius * Math.cos(new_angle))) - (calcwidth / 2);
                    newY = (pointerY + -(radius * Math.sin(new_angle))) - 32;
                }

                annotations[i].setAttribute("transform", "translate("
                    + newX + " " + newY + ")");
            }
        }
    },

    scaleAnnotations: function(event, scale) {
        var tgroup = element('annotations');

        var cX = element('map-container').offsetWidth / 2;
        var cY = element('map-container').offsetHeight / 2;

        if (tgroup) {
            var annotations = tgroup.childNodes;
            for (var i = 0; i < annotations.length; i++) {
                var calcwidth = annotations[i].getAttribute("calcwidth");
                var matrix = annotations[i].getCTM();

                var referenceX = (matrix.e + (calcwidth / 2));
                var referenceY = (matrix.f + 32);

                var ra = this.getRadiusAngle(referenceX, referenceY, cX, cY);

                var newX = (cX + ((ra.radius * scale) * Math.cos(ra.angle))) - (calcwidth / 2);
                var newY = (cY + -((ra.radius * scale) * Math.sin(ra.angle))) - 32;

                annotations[i].setAttribute("transform", "translate("
                    + newX + " " + newY + ")");
            }
        }
    },

    moveAnnotations: function(event) {
        var tgroup = element('annotations');
        if (tgroup) {
            var annotations = tgroup.childNodes;
            for (var i = 0; i < annotations.length; i++) {
                var matrix = annotations[i].getCTM();

                var original = $M([[matrix.a, matrix.c, matrix.e], [matrix.b, matrix.d, matrix.f], [0, 0, 1]]);
                var moved = $M([[1, 0, event.clientX - this.map_startX], [0, 1, event.clientY - this.map_startY], [0, 0, 1]]);

                var updated = moved.x(original);
                annotations[i].setAttribute("transform", "matrix(" +
                    updated.e(1, 1) + " " + updated.e(2, 1) + " " +
                    updated.e(1, 2) + " " + updated.e(2, 2) + " " +
                    updated.e(1, 3) + " " + updated.e(2, 3) + ")");
            }
        }
    },

    grabSlider: function(event, mapController) {
        var slider_handle = document.getElementById('slider');

        var matrix = slider_handle.getTransformToElement(slider_handle.parentNode);

        this.slider_xd1 = parseFloat(matrix.e);
        this.slider_yd1 = parseFloat(matrix.f) - event.clientY;
        this.slider_startY = event.clientY;

        this.boundSliderMove = this.moveSlider.bindAsEventListener(this, mapController);
        this.boundSliderRelease = this.releaseSlider.bindAsEventListener(this, mapController);
        var move = this.boundSliderMove;
        var release = this.boundSliderRelease;

        Event.observe(window, 'mousemove', move);
        Event.observe(window, 'mouseup', release);

        event.preventDefault();
    },

    moveSlider: function(event, mapController) {
        var delta;
        var dep_x;
        var dep_y;

        dep_x = this.slider_xd1;

        if ((event.type == "mousewheel") || (event.type == "DOMMouseScroll")) {
            var matrix = element('slider').getTransformToElement(element('slider').parentNode);

            this.slider_xd1 = parseFloat(matrix.e);
            this.slider_yd1 = parseFloat(matrix.f);
            this.slider_startY = event.clientY;

            var wheelDelta = this.wheelDelta(event);
            if (wheelDelta > 0) {
                dep_y = mapController.slider_yd1 + 1;
                delta = 0.95
            }
            if (wheelDelta < 0) {
                dep_y = mapController.slider_yd1 - 1;
                delta = 1.05;
            }
        } else {
            delta = 1 - ((event.clientY - mapController.slider_startY) * 0.05);
            mapController.slider_startY = event.clientY

            // slider_position holds the persistent y-axis offset pixel position of the slider handle
            // note that if  this is not a mousewheel event, slider_position is updated after the 25+/-pixel
            // constraint check further down.
            dep_y = mapController.slider_yd1 + event.clientY;
        }

        this.slider_position = dep_y;
        var slider_handle = element('slider');

        if (! (dep_y > 20) && !(dep_y < -20)) {
            slider_handle.setAttribute("transform", "translate(" + dep_x + " " + dep_y + ")");

            mapController.scaleMap(event, mapController, delta);
            event.preventDefault();
        }
    },

    scaleMap: function(event, mapController, delta) {
        var map = element('map-container');
        var content = element('map');
        var matrix = content.getCTM();

        var original = $M([[matrix.a, matrix.c, matrix.e], [matrix.b, matrix.d, matrix.f], [0, 0, 1]]);
        var scaled = $M([[delta, 0, 0], [0, delta, 0], [0, 0, 1]]);

        var shiftX = ((1 - delta) * map.offsetWidth) / 2;
        var shiftY = ((1 - delta) * map.offsetHeight) / 2;

        mapController.mapScale *= delta;

        var preshift = $M([[1, 0, shiftX], [0, 1, shiftY], [0, 0, 1]]);

        var updated = preshift.x(scaled.x(original));

        content.setAttribute("transform", "matrix(" +
            updated.e(1, 1) + " " + updated.e(2, 1) + " " +
            updated.e(1, 2) + " " + updated.e(2, 2) + " " +
            updated.e(1, 3) + " " + updated.e(2, 3) + ")");

        mapController.scaleAnnotations(event, delta);
    },

    wheelDelta: function(event) {
        return event.detail ? event.detail * -1: event.wheelDelta / 40;
    },

    captureScroll: function(event, mapController) {
        if (event.altKey) {
            this.rotateMap(event);
        } else {
            this.moveSlider(event, mapController);
        }
    },

    rotateMap: function(event) {
        if ((event.type == "mousewheel") || (event.type == "DOMMouseScroll")) {
            var wheelDelta = this.wheelDelta(event);
            var rotation = 0;
            if (wheelDelta > 0) {
                rotation = -3;
            }
            if (wheelDelta < 0) {
                rotation = 3;
            }

            var content = document.getElementById('map');
            var matrix = content.getCTM();

            var pointerX,
            pointerY;

            var leftVal = content.parentNode.parentNode.offsetLeft;
            var topVal = content.parentNode.parentNode.offsetTop;
            var parent = content.parentNode.parentNode.offsetParent;

            while (parent != null) {
                leftVal += parent.offsetLeft;
                topVal += parent.offsetTop;
                parent = parent.offsetParent;
            }

            pointerX = event.clientX - leftVal;
            pointerY = event.clientY - topVal;

            var radians = rotation * (Math.PI / 180);
            var cos = Math.cos(radians);
            var sin = Math.sin(radians);
            var original = $M([[matrix.a, matrix.c, matrix.e], [matrix.b, matrix.d, matrix.f], [0, 0, 1]]);

            var preshift = $M([[1, 0, -pointerX], [0, 1, -pointerY], [0, 0, 1]]);
            var rotated = $M([[cos, -sin, 0], [sin, cos, 0], [0, 0, 1]]);
            var postshift = $M([[1, 0, pointerX], [0, 1, pointerY], [0, 0, 1]]);

            var updated = postshift.x(rotated.x(preshift.x(original)));

            // Remove annotations
            this.rotateAnnotations(event, pointerX, pointerY, radians);

            content.setAttribute("transform", "matrix(" +
                updated.e(1, 1) + " " + updated.e(2, 1) + " " +
                updated.e(1, 2) + " " + updated.e(2, 2) + " " +
                updated.e(1, 3) + " " + updated.e(2, 3) + ")");

            event.preventDefault();
        }

    },

    releaseSlider: function(event, mapController) {
        Event.stopObserving(window, 'mousemove', mapController.boundSliderMove);
        Event.stopObserving(window, 'mouseup', mapController.boundSliderRelease);
    },

    getStyleElement: function(style, element) {
        var elements = style.split(";");
        for (var i = 0; i < elements.length; i++) {
            var subelements = elements[i].split(":");
            if (subelements[0].trim() == element.trim()) {
                return subelements[1];
            }
        }
        return "";
    },

    setStyleElement: function(style, element, setting) {
        var ret = "";

        if (style) {
            var elements = style.split(";");
            var found = false;
            for (var i = 0; i < elements.length; i++) {
                var subelements = elements[i].split(":");
                if (subelements[0].trim() == element.trim()) {
                    ret += subelements[0].trim() + ":" + setting;
                    found = true;
                    continue;
                }
                ret += (elements[i] + ";");
            }

            if (!found) {
                ret += element + ":" + setting;
            }
        } else {
            ret += element + ":" + setting;
        }

        return ret;
    }
});

