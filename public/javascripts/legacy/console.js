var ConsoleController = Class.create({
    initialize: function() {
        this.loading = false;    
    },

    openChart: function(event) {
        if(event.element().readAttribute('chart') == 'area') {
            areaChartRequester.getChart(event);
        } else if(event.element().readAttribute('chart') == 'map') {
            mapRequester.getMap(event, false);
        } else if(event.element().readAttribute('chart') == 'table') {
            this.getFlows();
        }
    },

    getPattern: function(event, input) {
        var element;
        if(event) element = event.element();

        var hours, mins, days, start;

        if(element) {
            hours = element.readAttribute("hours");
            mins = element.readAttribute("mins");
            days = element.readAttribute("days");
            start = element.readAttribute("start");
        }

        var pattern = "pattern=";
        if(hours || mins || days || start) {
            var request = "";

            if(days) request += ("days " + days + " ");
            else if(mins) request += ("minutes " + mins + " ");
            else if(hours) request += ("hours " + hours + " ");

            if(start) request += ("start " + start + " ");

            pattern += Base64.encode(request);
        } else if(input) {
            pattern += Base64.encode(input);
        }

        return pattern;
    },

    updateFrame: function() {
        try {
            $('page').style.height = window.innerHeight + "px";
        } catch (e) {
            alert("Error updateFrame(1):" + e.message);
        }
        
        try {
            var window_height = $('page').offsetHeight;
            var window_width = $('page').offsetWidth;
            var divider_height = $('divider').offsetHeight;
        } catch (e) {
            alert("Error updateFrame(1.5):" + e.message);
        }

        try {
            // Set the actual variable object's height
            $('content').style.height = (window_height - divider_height) + "px";
            $('content').style.width = (window_width) + "px";
        } catch (e) {
            alert("Error updateFrame(2):" + e.message);
        }

        try {
            // Set the height of the div containing the variable object
            $('main').style.height = (window_height - divider_height) + "px";
            $('main').style.width = (window_width) + "px";
            $('main').style.marginLeft = 0 + "px";
        } catch (e) {
            alert("Error updateFrame(3):" + e.message);
        }

        try {
            if($('inspector')) {
                $('inspector').style.left = $('map-container').offsetWidth - $('inspector').offsetWidth  - 2 + "px";
                $('inspector').style.height = $('map-container').offsetHeight - 2 + "px";
            }
        } catch (e) {
            alert("Error updateFrame(5):" + e.message);
        }

        try {
            if($('detailed-flows')) {
                var flows_height = 0.3 * $('main').offsetHeight;
                $('detailed-flows').style.overflow = "hidden";          
                $('detailed-flows').style.top = $('main').offsetHeight - (flows_height) + "px";
                $('detailed-flows').style.height = flows_height + "px";

                var heading_offset = $('flow-header').offsetHeight + $('flow-footer').offsetHeight;
                $('flow-table').style.height = (flows_height - heading_offset) + "px";
                // The 30 above is a guess, the real figure should be something that accounts for the height of the
                // header and the height of the footer.
                $('flow-table').style.overflow = "auto";
                $('detailed-flows').style.width = window_width + "px";

                if($('detailed-flows').style.display == "block") {
                    $('content').style.height = window_height - divider_height - flows_height + "px";
                }
            }
        } catch (e) {
            alert("Error updateFrame(6):" + e.message);
        }

        $('volume-container').style.width = ($('content').offsetWidth - 10) + "px";
        $('volume-container').style.height = "50px";
        
        try {
            if(controller.volumeTracker) {
                if(element('svg-' + controller.volumeTracker)) {
                    element('svg-' + controller.volumeTracker).setAttributeNS(null, 'width', $('volume-container').offsetWidth);
                }
            }
        } catch (e) {
            alert("Error updateFrame(6.5):" + e.message);
        }
        
        try {
            element('map-container').style.width = $('content').offsetWidth + "px";
            element('map-container').style.height = ($('content').offsetHeight - $('volume-container').offsetHeight - 10) + "px";
        } catch (e) {
            alert("Error updateFrame(7):" + e.message);
        }

        try {
            element('maploader').setAttribute("transform", "translate(" + ($('content').offsetWidth / 2) + " " + (($('content').offsetHeight - $('volume-container').offsetHeight) / 2) + ")");
        } catch (e) {
            alert("Error updateFrame(8):" + e.message);
        }

    },

    checkEnter: function(event) {
        var Charts = {
            "map":1,
            "area":2,
            "flow":3,
            "all":4
        };
        var chart;

        if(event && event.which == 13) {
            var tokens = event.target.value.split(" ");

            for (var i = 0; i < tokens.length; i++) {
                switch(tokens[i]) {
                    case "map":
                        chart = Charts.map;
                        break;
                    case "area":
                        chart = Charts.area;
                        break;
                    case "flows":
                        chart = Charts.flow;
                        break;
                    case "all":
                        chart = Charts.all;
                        break;
                }
            }

            if(!chart) {
                chart = Charts.map;
            }

            switch(chart) {
                case Charts.area:
                    tokens.splice(tokens.indexOf("area"), 1);
                    areaChartRequester.getChart(null, tokens.join(" "));
                    break;
                case Charts.map:
                    tokens.splice(tokens.indexOf("map"), 1);
                    mapRequester.getMap(null, tokens.join(" "));
                    break;
                case Charts.flow:
                    tokens.splice(tokens.indexOf("flows"), 1);
                    this.getFlows(null, tokens.join(" "));
                    break;
                case Charts.all:
                    tokens.splice(tokens.indexOf("all"), 1);
                    this.getFlows(null, tokens.join(" "));
                    mapRequester.getMap(null, tokens.join(" "));
                    areaChartRequester.getChart(null, tokens.join(" "));
                    break;
            }

            event.target.blur();
            return false;
        } else {
            return true;
        }
    },

    clearText: function(event) {
        if(event.target.value == event.target.getAttribute("default")) {
            event.target.value = "";
        }
    },

    setDefault: function(event) {
        if(event.target.value == "") {
            event.target.value = event.target.getAttribute("default");
        }
    },

    openDialog: function(event) {
        var link = event.element();
        var url;
        if((link.readAttribute('action') == 'create') && !$('createuser')) {
            url = "fragments/createuser.html";
            new Ajax.Request(url, {
                method: 'get',
                onSuccess: function(transport) {
                    administrationController.createUserWindow(transport);
                }
            });
        } else if ((link.readAttribute("action") == 'update') && !$('updateuser')) {
            url = "fragments/updateuser.jsp";
            new Ajax.Request(url, {
                method: 'get',
                onSuccess: function(transport) {
                    administrationController.updateUserWindow(transport);
                }
            });
        } else if ((link.readAttribute("action") == 'users') && !$('userlist')) {
            url = "fragments/users.html";
            new Ajax.Request(url, {
                method: 'get',
                onSuccess: function(transport) {
                    administrationController.usersWindow(transport);
                }
            });
        } else if ((link.readAttribute("action") == 'networks') && !$('networklist')) {
            url = "fragments/networks.html";
            new Ajax.Request(url, {
                method: 'get',
                onSuccess: function(transport) {
                    administrationController.networksWindow(transport);
                }
            });
        } else if ((link.readAttribute("action") == 'addNetwork') && !$('addnetwork')) {
            url = "fragments/addnetwork.html";
            new Ajax.Request(url, {
                method: 'get',
                onSuccess: function(transport) {
                    administrationController.addNetworkWindow(transport);
                }
            });
        } else if (link.readAttribute("action") == 'logout') {
            navigateTo('logout');
        } else if ((link.readAttribute("action") == 'groups') && !$('grouplist')) {
            url = "fragments/directorygroups.html";
            new Ajax.Request(url, {
                method: 'get',
                onSuccess: function(transport) {
                    administrationController.groupsWindow(transport);
                }
            });
        } else if ((link.readAttribute("action") == 'addGroup') && !$('addgroup')) {
            url = "fragments/addgroup.html";
            new Ajax.Request(url, {
                method: 'get',
                onSuccess: function(transport) {
                    administrationController.addGroupWindow(transport);
                }
            });
        } else if ((link.readAttribute("action") == 'alerts') && !$('alertconsole')) {
            url = "fragments/alertconsole.html";
            
            new Ajax.Request(url, {
                method: 'get',
                onSuccess: function(transport) {
                    controller.idsConsole(transport);
                }
            });
        } else if ((link.readAttribute("action") == 'wafalerts') && !$('wafconsole')) {
            url = "fragments/wafconsole.html";

            new Ajax.Request(url, {
                method: 'get',
                onSuccess: function(transport) {
                    controller.wafConsole(transport);
                }
            });
        }
    },

    reset: function(event) {
        var form = event.target.parentNode.parentNode.parentNode.parentNode;
        form.reset();
    },

    idsConsole: function(req) {
        if(req.responseText != null) {
            var content = windowController.openWindow("Alert Console", "alertconsole", 800, 300);
            content.innerHTML = req.responseText;

            this.populateAlerts();
            
            content.parentNode.style.top = (window.innerHeight/2) - (content.parentNode.offsetHeight/2) + "px";
            content.parentNode.style.left = (window.innerWidth/2) - (content.parentNode.offsetWidth/2) + "px";

            Event.observe($('alertlist'), 'keydown', controller.navigateAlerts.bindAsEventListener(this));
        }
    },

    wafConsole: function(req) {
        if(req.responseText != null) {
            var content = windowController.openWindow("Web Application Firewall Console", "wafconsole", 800, 300);
            content.innerHTML = req.responseText;

            this.populateWAFAlerts();

            content.parentNode.style.top = (window.innerHeight/2) - (content.parentNode.offsetHeight/2) + "px";
            content.parentNode.style.left = (window.innerWidth/2) - (content.parentNode.offsetWidth/2) + "px";

            Event.observe($('waflist'), 'keydown', controller.navigateAlerts.bindAsEventListener(this));
        }
    },

    navigateAlerts: function(event) {
        if(event.keyCode == 40 || event.keyCode == 38) {
            var rows = event.element().getElementsByTagName('tr');

            for(var i = 0; i < rows.length; i++) {
                if(rows[i].getAttribute("class") == "selected") {
                    if(event.keyCode == 40 && i < (rows.length - 1)) {
                        this.alertDetail(event, rows[++i]);
                    } else if(event.keyCode == 38 && i > 0) {
                        this.alertDetail(event, rows[--i]);
                    }

                    event.stopPropagation();
                    event.preventDefault();

                    var scrollRow;
                    if(rows[i - 2]) scrollRow = rows[i - 2];
                    else if(rows[i - 1]) scrollRow = rows[i - 1];
                    else scrollRow = rows[i];

                    event.element().parentNode.parentNode.scrollTop = scrollRow.offsetTop;
                    break;
                }
            }
        }
        return false;
    },

    populateAlerts: function() {
        var url = "alerts";
        
        var post = this.getPattern(null, "minutes 15");
        post += "&type=snort";
        
        new Ajax.Request(url, {
            method: 'post',
            postBody: post,
            onSuccess: function(transport) {
                controller.populateAlertsHandler(transport);
            }
        })
    },

    populateWAFAlerts: function() {
        var url = "alerts";
        
        var post = this.getPattern(null, "minutes 15");
        post += "&type=modsecurity";
        
        new Ajax.Request(url, {
            method: 'post',
            postBody: post,
            onSuccess: function(transport) {
                controller.populateWAFAlertsHandler(transport);
            }
        })
    },

    populateAlertsHandler: function(req) {
        if(req.responseXML != null) {
            var root = req.responseXML.documentElement;
            var alerts = root.getElementsByTagName("alert");

            $('alertlist').innerHTML = '';
            for (var i = (alerts.length - 1); i >= 0; i--) {
                var row = document.createElement("tr");
                $('alertlist').appendChild(row);

                row.setAttribute("data", alerts[i].childNodes[0].nodeValue);
                
                var date = document.createElement("td");
                row.appendChild(date);

                var alertDate = new Date((alerts[i].getAttribute("date") * 1000));
                date.appendChild(document.createTextNode(alertDate.format("m/dd/yy HH:MM:ss")));

                var message = document.createElement("td");
                row.appendChild(message);
                
                message.appendChild(document.createTextNode(alerts[i].getAttribute("message")));

                //var nodes = document.createElement("td");
                //row.appendChild(nodes);

                row.setAttribute("src", alerts[i].getAttribute("src"));
                row.setAttribute("dst", alerts[i].getAttribute("dst"));
                row.setAttribute("sport", alerts[i].getAttribute("sport"));
                row.setAttribute("dport", alerts[i].getAttribute("dport"));
                //nodes.appendChild(document.createTextNode(alerts[i].getAttribute("src") + " > " + alerts[i].getAttribute("dst")));

                var actions = document.createElement("td");
                row.appendChild(actions);

                var deleteAction = document.createElement("a");
                actions.appendChild(deleteAction);

                deleteAction.setAttribute("action", "delete");
                deleteAction.setAttribute("href", "#");
                deleteAction.setAttribute("record", alerts[i].getAttribute("record"));
                deleteAction.appendChild(document.createTextNode("Delete"));

                Event.observe(deleteAction, 'click', this.deleteAlert.bindAsEventListener(this));
                Event.observe(row, 'click', this.alertDetail.bindAsEventListener(this, row));
            }
        }
    },

    populateWAFAlertsHandler: function(req) {
        if(req.responseXML != null) {
            var root = req.responseXML.documentElement;
            var alerts = root.getElementsByTagName("alert");

            $('waflist').innerHTML = '';
            for (var i = (alerts.length - 1); i >= 0; i--) {
                var row = document.createElement("tr");
                $('waflist').appendChild(row);
                var log = JSON.parse(alerts[i].childNodes[0].nodeValue);

                var date = document.createElement("td");
                row.appendChild(date);

                var alertDate = new Date((alerts[i].getAttribute("date") * 1000));
                date.appendChild(document.createTextNode(alertDate.format("m/dd/yy HH:MM:ss")));

                var message = document.createElement("td");
                row.appendChild(message);

                message.appendChild(document.createTextNode(Base64.decode(log.A)));

                //var nodes = document.createElement("td");
                //row.appendChild(nodes);

                row.setAttribute("src", alerts[i].getAttribute("src"));
                row.setAttribute("dst", alerts[i].getAttribute("dst"));
                row.setAttribute("sport", alerts[i].getAttribute("sport"));
                row.setAttribute("dport", alerts[i].getAttribute("dport"));
                //nodes.appendChild(document.createTextNode(alerts[i].getAttribute("src") + " > " + alerts[i].getAttribute("dst")));

                var actions = document.createElement("td");
                row.appendChild(actions);

                var deleteAction = document.createElement("a");
                actions.appendChild(deleteAction);

                deleteAction.setAttribute("action", "delete");
                deleteAction.setAttribute("href", "#");
                deleteAction.setAttribute("record", alerts[i].getAttribute("record"));
                deleteAction.appendChild(document.createTextNode("Delete"));

                Event.observe(deleteAction, 'click', this.deleteAlert.bindAsEventListener(this));
            //Event.observe(row, 'click', this.alertDetail.bindAsEventListener(this, row));
            }
        }
    },

    alertDetail: function(event, row) {
        var rows = $('alertconsole').getElementsByTagName('tr');

        for(var i = 0; i < rows.length; i++) {
            rows[i].removeAttribute("class");
        }
        
        row.setAttribute("class", "selected");
        $('alertsource').setAttribute("value", row.getAttribute('src'));
        $('alertdestination').setAttribute("value", row.getAttribute('dst'));
        
        if(row.getAttribute('sport') != 'null') {
            $('alertsport').setAttribute("value", row.getAttribute('sport'));
        } else {
            $('alertsport').setAttribute("value", "");
        }

        if(row.getAttribute('dport') != 'null') {
            $('alertdport').setAttribute("value", row.getAttribute('dport'));
        } else {
            $('alertdport').setAttribute("value", "");
        }

        $('alertpacket').getElementsByTagName('p')[0].innerHTML = '';

        var sanitized = "";
        var data = Base64.decode(row.getAttribute('data'));
        for(i = 0; i < data.length; i++) {
            if(Convert.toDec(data.substr(i, 1)) < 33 || Convert.toDec(data.substr(i, 1)) > 126) {
                sanitized = sanitized + ".";
            } else {
                sanitized = sanitized + data.substr(i, 1);
            }
        }

        var asciiLines = new Array();
        var hexLines = new Array();

        var asciiFormatted = "";
        var hexFormatted = "";
        for(i = 0; i < sanitized.length; i++) {
            if(i != 0 && i % 16 == 0) {
                asciiLines.push(asciiFormatted);
                hexLines.push(hexFormatted);
                asciiFormatted = "";
                hexFormatted = "";
            } else if(i != 0 && i % 8 == 0) {
                asciiFormatted += " ";
                hexFormatted += " ";
            }

            asciiFormatted += sanitized.charAt(i);

            var bt = data.charCodeAt(i) & 0xFF;
            var hex = bt.toString(16).toUpperCase();
            if(hex.length == 1) hex = "0" + hex;
            hexFormatted += (hex + " ");
        }

        var finalLines = new Array();
        for(i = 0; i < asciiLines.length; i++) {
            finalLines.push(hexLines[i] + " " + asciiLines[i]);
        }

        var pre = document.createElement("pre");
        pre.appendChild(document.createTextNode(finalLines.join("\n")));
        $('alertpacket').getElementsByTagName('p')[0].appendChild(pre);
    },

    deleteAlert: function(event) {
        var url = "alerts";
        var post = "action=delete&record=" + event.element().readAttribute('record');
        new Ajax.Request(url, {
            method: 'post',
            postBody: post,
            onSuccess: function(transport) {
                controller.populateAlerts();
            }
        });
    }  
});

var controller, windowController, mapRequester, areaChartRequester, administrationController, flowRequester;
function init() {
    try {
        controller = new ConsoleController($('menu'));
        window.addEventListener('resize', controller.updateFrame, false);
    } catch (e) {
        alert("Error init(1): " + e.message);
    }
    
    try {
        mapRequester = new MapRequester(controller);
        mapRequester.getMap();
    } catch (e) {
        alert("Error init(2): " + e.message);
    }
        
    try {
        flowRequester = new FlowRequester(controller, $('flow-data'));
        flowRequester.getFlows();
    } catch (e) {
        alert("Error init(3): " + e.message);
    }

    try {
        areaChartRequester = new AreaChartRequester(controller);
        areaChartRequester.requestData("days 1", $('page').offsetWidth - 10, 50);
    } catch (e) {
        alert("Error init(3.5): " + e.message);
    }
    
    try {
        administrationController = new AdministrationController();
    } catch (e) {
        alert("Error init(4): " + e.message);
    }
    
    try {
        controller.updateFrame();
        windowController = new WindowController();
    } catch (e) {
        alert("Error init(5): " + e.message);
    }   
}

function navigateTo(url) {
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", CONTEXT_ROOT + "/" + url);

    document.body.appendChild(form);
    form.submit();
}

var dateFormat = function () {
    var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
    timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
    timezoneClip = /[^-+\dA-Z]/g,
    pad = function (val, len) {
        val = String(val);
        len = len || 2;
        while (val.length < len) val = "0" + val;
        return val;
    };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var	_ = utc ? "getUTC" : "get",
        d = date[_ + "Date"](),
        D = date[_ + "Day"](),
        m = date[_ + "Month"](),
        y = date[_ + "FullYear"](),
        H = date[_ + "Hours"](),
        M = date[_ + "Minutes"](),
        s = date[_ + "Seconds"](),
        L = date[_ + "Milliseconds"](),
        o = utc ? 0 : date.getTimezoneOffset(),
        flags = {
            d:    d,
            dd:   pad(d),
            ddd:  dF.i18n.dayNames[D],
            dddd: dF.i18n.dayNames[D + 7],
            m:    m + 1,
            mm:   pad(m + 1),
            mmm:  dF.i18n.monthNames[m],
            mmmm: dF.i18n.monthNames[m + 12],
            yy:   String(y).slice(2),
            yyyy: y,
            h:    H % 12 || 12,
            hh:   pad(H % 12 || 12),
            H:    H,
            HH:   pad(H),
            M:    M,
            MM:   pad(M),
            s:    s,
            ss:   pad(s),
            l:    pad(L, 3),
            L:    pad(L > 99 ? Math.round(L / 10) : L),
            t:    H < 12 ? "a"  : "p",
            tt:   H < 12 ? "am" : "pm",
            T:    H < 12 ? "A"  : "P",
            TT:   H < 12 ? "AM" : "PM",
            Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
            o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
            S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
        };

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();

// Some common format strings
dateFormat.masks = {
    "default":      "ddd mmm dd yyyy HH:MM:ss",
    shortDate:      "m/d/yy",
    mediumDate:     "mmm d, yyyy",
    longDate:       "mmmm d, yyyy",
    fullDate:       "dddd, mmmm d, yyyy",
    shortTime:      "h:MM TT",
    mediumTime:     "h:MM:ss TT",
    longTime:       "h:MM:ss TT Z",
    isoDate:        "yyyy-mm-dd",
    isoTime:        "HH:MM:ss",
    isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
    "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
    "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
    "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ]
};


// For convenience...
Date.prototype.format = function (mask, utc) {
    return dateFormat(this, mask, utc);
};

var Convert = {
    chars: " !\"#$%&amp;'()*+'-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~",
    hex: '0123456789ABCDEF',
    bin: ['0000', '0001', '0010', '0011', '0100', '0101', '0110', '0111', '1000', '1001', '1010', '1011', '1100', '1101', '1110', '1111'],
    decToHex: function(d){
        return (this.hex.charAt((d - d % 16)/16) + this.hex.charAt(d % 16));
    },
    toBin: function(ch){
        var d = this.toDec(ch);
        var l = this.hex.charAt(d % 16);
        var h = this.hex.charAt((d - d % 16)/16);
        var hhex = "ABCDEF";
        var lown = l < 10 ? l : (10 + hhex.indexOf(l));
        var highn = h < 10 ? h : (10 + hhex.indexOf(h));
        return this.bin[highn] + ' ' + this.bin[lown];
    },
    toHex: function(ch){
        return this.decToHex(this.toDec(ch));
    },
    toDec: function(ch){
        var p = this.chars.indexOf(ch);
        return (p <= -1) ? 0 : (p + 32);
    }
};

function element(name) {
    return document.getElementById(name);
}

function generateID(string) {
    return "A" + Base64.encode(string).replace(/[= \._]/g, '')
}

function log1p (x) {
    var ret = 0,        n = 50; // degree of precision
    if (x <= -1) {
        return '-INF'; // JavaScript style would be to return Number.NEGATIVE_INFINITY
    }
    if (x < 0 || x > 1) {
        return Math.log(1 + x);
    }
    for (var i = 1; i < n; i++) {
        if ((i % 2) === 0) {
            ret -= Math.pow(x, i) / i;
        } else {
            ret += Math.pow(x, i) / i;
        }
    }
    return ret;
}