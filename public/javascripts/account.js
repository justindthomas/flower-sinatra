$(document).ready(function() {
    window.flower = {}
    wf = window.flower

    $.get("/users",function(data) {
        var users = JSON.parse(data)
        for(var idx in users) {
            $(".organization ul").append("<li>" +
                                         users[idx]["username"] +
                                         "<button username=\"" +
                                         users[idx]["username"] +
                                         "\">X</button></li>")
        }
    
        $(".organization li button").one("click", function() {
            var username = $(this).attr("username")

            var item = $(this).parent()

            $.ajax({
                url: '/account',
                type: 'DELETE',
                data: "{\"username\":\"" + username + "\"}",
                contentType:'application/json',
                dataType: 'text',
                success: function(result) { item.remove() },
                error: function(result){ console.log("failed to delete user") }
            })
        })
    })
})
